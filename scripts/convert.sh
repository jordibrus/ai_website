#!/usr/bin/env bash

set -e

SRC=aiwebsite/templates
LANGS=(cat de en es fr it)

mkdir -p ca services/hosted stuff/man_anon stuff/man_blog stuff/man_ca \
      stuff/man_irc stuff/man_jabber stuff/man_mail stuff/man_web who/rplan

convert_page() {
    # convert to markdown (a recent pandoc version is required; i.e. not debian)
    # --no-wrap ?
    pandoc -f html -t markdown_strict \
           --parse-raw \
           --normalize \
           --preserve-tabs \
           --columns=140 \
           $1 -o $2
            
    # Clean Jinja header and footer; remove {{ lang }} jinja code
    perl -pi -e 's/^{% extends "\\_base\.html" %} {% block title %}([^{]+).*/---\ntitle: "$1"\n---\n/; s/^{% endblock %}$//; s@{{.*lang.*}}/@@g' $2
    # fix "{{ lang }}" links; clean an unwanted "\" from title
    perl -pi -e 's@\(/%7B%7B%20lang%20%7D%7D([^)]+)@($1@g; s@\\@@g if 1 .. 4 && /^title: /' $2
}

if [[ "$1" != "" && "$2" != "" ]]; then
	convert_page $1 $2
else
	for lang in ${LANGS[*]}; do
		find $SRC/$lang -name '*.html' -print0 |
		while IFS= read -r -d $'\0' page; do
			out=${page#$SRC/$lang/}
			out=${out%.*}.$lang.md
			echo "in: $page out: $out"

			convert_page $page $out

			if (($(stat -f "%z" $out) < 5)); then
				rm $out
			fi
		done
	done
fi
