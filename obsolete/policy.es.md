title: Política
----

Política
========

Para participar y tener un servicio en nuestra red tienes que compartir nuestros pincipios de **anti-facismo, anti-racismo, anti-sexismo, anti-homofobia, anti-transfobia y anti-militarismo**. Tus proyectos tienen que tener los mismos principios **no comerciales** que mantienen vivo a nuestro proyecto, y tener
la voluntad de compartir experiencias y luchas, con toda la paciencia que ello implica ;))))

Cualquier servicio alojado en nuestros servidores no puede estar destinado (directa o indirectamente) a actividades comerciales, religiones,
partidos políticos -institucionales o no- o, para decirlo rápidamente, por cualquiera que ya tiene medios y recursos para difundir
ampliamente sus ideas, o quienes hacen uso del concepto de representación y delegación (explícita o implícitamente) en sus relaciones y
proyectos del día a día.

El servidor sólo conserva aquellos logs que son estríctamente necesarios para operaciones de depuración, y dichos logs no conservan ningún
tipo de información personal que relacione cuentas con identidades.

Política para servicios específicos
-----------------------------------

- [Correo electrónico (incluyendo listas de correo y newsletters)](#mail)
- [Alojamiento web y blogs](#web)

--------------------------------------------------------------------------------------------------------------------------------------------

### <a name="mail"></a>Correo electrónico (incluyendo listas de correo y newsletters paa las cuestiones relacionados con el spam)

Abrir una cuenta en nuetsros servidores y servicios implica que compartes nuestro [manifesto](/who/manifesto) y que tomas en
consideración lo siguiente:

- **Límites de espacio:** El uso de tu cuenta está limitado por el respeto a todas las peronas que usan nuestros servicios. Los recursos
  son escasos, el servicio es gratuito y l\*s usuari\*s much \*s. Por ello, invitamos a que hagan un uso limitado de nuestros
  discos, descargando o eliminando los correos tan
  seguido como sea posible.
- **Uso/No-uso de este servicio:** Las cuentas de correo que no se hayan usado en 365 días serán desactivadas. Si piensas que no vas a
    usar tu cuenta por 12 meses pero aún así quieres conservarla, [escríbenos](mailto:info@autistici.org).
- **Uso de WebMail:** El WebMail consume muchos recursos, por lo que debería ser usado sólo en casos de emergencia; para un uso normal, te
    invitamos a usar un cliente de correo y descargarlo en tu PC.
- **Contraseña:** Por cuestiones de privacidad, nunca rastreamos nada de las personas que solicitan la activación de una cuenta. Para usar
    tu correo electrónico, tendrás que elegir una simple pregunta que te permitirá recobrar tu contraseña en el caso de que la pierdas
    u olvides. Si también pierdes la pregunta, o la respuesta a la pregunta, no seremos capaces de reenviarte la información para que puedas
    volver a acceder a tu cuenta. La única cosa que seremos capaces de hacer será abrirte una nueva cuenta, pero no recobraremos tus cosas
    viejas.
     Entonces, recuerda configurar tu "pregunta de recuperación" en tu cuenta haciendo click en el link que encontrarás en tu panel de
    usuari\*
- **Spam y virus:** El servidor usa herramientas de antivirus y antispam pero, al igual que muchos otros servicios comerciales, son
    poco efectivos. A todos los correos se les asigna una puntuación en base a algunas reglas bastante establecidas sobre el spam. Si algún
    correo electrónico recibe una puntuación lo suficientemente elevada, se le agrega, en el Asunto, la etiqueta \*\*\*SPAM\*\*\*. Luego
    podrás configurar tu cliente de email para que filtre esos mensajes. De todas formas, te recomendamos que mires los emails que son
    "etiquetados" como SPAM, porque el filtro no es infalible.

- **Envío de Spam y virus:** Para evitar tener nuestros servidores incluidos en las listas negras de medio mundo, no queremos que nuestras
    cuentas de email sean usadas para enviar spam o virus. Cualquier cuenta que haga esto será desactivada sin mayor noticia.
- **Responsabilidades legales:** Respecto a todo lo demás que hagas. Nuestros servidores no son responsables de lo que escribes, ni por la
    salvaguarda de tu propia privacidad. Por ello te invitamos a que uses todas las
    [herramientas](/docs/mail/privacymail "protect your privacy") que existen para proteger tu privacidad.

--------------------------------------------------------------------------------------------------------------------------------------------

### <a name="web"></a>Alojamiento web y blogs

- La responsabilidad del contenido que hay en un sitio recae en el/la webmaster de ese sitio. El colectivo que llevamos
    Autistici-Inventati no puede asumir la responsabilidad por los contenidos alojados en cada uno de los sitios.
     Por ello, invitamos a tod\*s l\*s webmasteres a respetar nuestros principios y sólo
    solicitar el servicio una vez hayan leído y estén de acuerdo con nuestro [manifesto](/who/manifesto "manifesto").
- El espacio web será otorgado únicamente a través de la aprobación del colectivo. Sitios web personales no serán alojados a no ser que
    publiquen contenido y documentos de particular interés de acuerdo a nuestro [manifesto](/who/manifesto "manifesto") y a la
    opinión del colectivo.
- Te recordamos que no subas al sitio materiales protegido por copyright (como algunos mp3 y divx) que puedan poner en peligro nuestros
    servidores (y todos los servicios conectados)
- Cuando trabajes en tu sitio, piensa un poco en la batalla que estamos luchando por un conocimiento colaborativo libre. Esperamos que
    seas sensible a éstas batallas. Piensa en nosotr\*s cuando veas esa pequeña, pero significativa, marca de
    "copyright" en el pié de tu página.
     El material publicado en nuestros servidores tendrá que ser estar disponible cuando menos con una **licencia libre** (como por ejemplo:
    [GPL](http://wwww.gnu.org/), o [Creative Commons](http://www.creativecommons.it/)) -- si acaso decides usar alguna licencia con
    copyright ;)
- Los límites del espacio web son los de los recursos de la máquina. Esto significa que no es una buena idea usar estos espacios para
    datos personales, o archivos de gran tamaño, a no ser que sea estrictamente necesario. **Generalmente, confiamos en la colaboración
    y responsabilidad.**.
     **Si tienes archivos muy grandes** que tener en línea, te invitamos a contactar con nosotr\*s para verificar
    el espacio disponible antes de subir el archivo.
- A/I intenta salvaguradar la privacidad y anonimidad de sus usuari\*s: Por lo tanto no está permitido utilizar
    contadores u otros servicios de estadística web (como shinystat o google analytics) en cualquier sitio web, porque éstos serviciios
    guardan las IP de las personas que visitan dicho sitio. Si quieres un contador, usa
    [Piwik](/services/piwik "Statistiche Piwik A/I"), nuestro nuevo servicio de estadísticas.
- Cuando trabajes en tu sitio, piensa que no todas las personas tienen la última tecnología, y por lo tanto quizás haya personas que no
    puedan ver tu sitio; para nosotr\*s la **accesibilidad** de los sitios web que alojamos es importante.(mira
    <http://www.ecn.org/xs2web> para tener más información).
- En la página de [propaganda](/who/propaganda "propaganda") de nuestro sitio encontrarás un pequeño logo del proyecto A/I,
    estaremos muy felices de ver que los sitios que alojamos lo dicen por alguna parte ;))))). Incluso, en el caso de que estés utilizando
    tu propio dominio (lee nuestras [notas técnicas](/docs/web/domains "Register your own domain")), el que incluyas un pequeño
    logo de Inventanti/Autistici en tu página principal es considerado como gesto pequeño pero importante. Muestra que compartes y apoyas
    nuestro proyecto.
- **No está permitido que se utilice el espacio asignado para redireccionar a otros sitios**, porque consideramos que esto es un gasto
    innecesario de nuestros recursos
- El espacio en los servidores que no haya sido utilizado en los 90 días posteriores a su activación serán desactivados y eliminados (por
    las mismas razones de antes)
- Si has solicitado el uso de MySQL, el mantenimiento de la base de datos se hace a través de la interfaz web  
     NO ES NECESARIO que instales ningún software como phpmyadmin (o similar) para manejar tus bases de datos, porque ya lo hemos intalado y
    lo tenemos bien cuidado y atendido.

- - -

Si tienes más dudas, puedes mirar nuestras [Preguntas frecuentes](/docs/faq) o [¡contactar con nostr\*s!](mailto:info@autistici.org).


