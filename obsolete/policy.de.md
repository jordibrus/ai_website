title: Richtlinien um von den A/I-Servern gehostet zu werden
----

Richtlinien um von den A/I-Servern gehostet zu werden
=====================================================

Um unsere Server nutzen zu können, müsst ihr die Prinzipien des **Antifaschismus, Antirassismus, Antisexismus, Antihomophobie, Antitransphobie und Antimilitarismus** teilen.
Eure Projekte müssen genauso **unkommerzieller Natur** sein wie das unsere. Dazu gehört auch ein Verlangen zum Teilen und Austausch von
Beziehungen und Kämpfen, mit all der Geduld, die das auch erfordert ;))))

Jeder Service, den wir auf unseren Servern anbieten, darf keinen kommerziellen, parteipolitischen oder religiösen Aktivitäten dienen, weder
direkt noch indirekt: um es kurz zu fassen, wir hosten niemanden der/die bereits Möglichkeiten und Ressourcen hat, um seine/ihre Ideen und
Inhalte zu verbreiten oder der/die Konzepte der Delegation und Repräsentation in Projekten und alltäglichen Beziehungen nutzt, explizit wie
implizit.

Der Server behält nur die Log-Dateien, die für Debug-Operationen unerlässlich sind. Persönliche Verbindungsdaten werden nicht gespeichert.

Richtlinien für die einzelnen Angebote
--------------------------------------

- [E-Mail (auch Mailinglistem und Newsletter)](#mail)
- [Web Hosting und Blogs](#web)

--------------------------------------------------------------------------------------------------------------------------------------------

### <a name="mail"></a>E-Mail-Richtlinien (auch Mailinglistem und Newsletter zum Thema Spam)

Einen Account auf unserem Server zu eröffnen, bedeutet das Teilen unseres [Manifestes](/who/manifesto) und auch die Kenntnisnahme
des Folgenden:

- **Speicherplatz**: Die Nutzung Eures Accounts wird beschränkt durch die Nutzung aller, die unsere Dienste in Anspruch nehmen. Die
    Ressourcen sind knapp, der Dienst ist frei zugänglich und User haben wir viele. Deshalb bitten wir Euch darum, den Bedarf Eures
    Speicherplatzes auf unseren Platten zu beschränken, indem möglichst oft die E-Mails von unseren Servern gelöscht werden (z.B. durch den
    Download derselben).
- **Nutzung/Nichtbenutzung dieses Dienstes**: Postfächer, die mehr als 365 Tage ungenutzt bleiben, werden deaktiviert. Wenn Ihr bereits
    wisst, dass Ihr den Account mehr als 12 Monate nicht nutzen werdet, er aber erhalten bleiben soll, dann [meldet Euch bei
    uns](mailto:info@autistici.org).
- **WebMail-Nutzung**: WebMail verbraucht einen großen Teil der Serverkapazität, und sollte deshalb nur im Notfall genutzt werden; für den
    alltäglichen Gebrauch empfehlen wir die Nutzung einer E-Mail-Software und den Download der E-Mails auf Eure Computer.
- **Passwort**: Um die Privatsphäre zu schützen, speichern wir nie Daten von jemanden, der/die die Aktivierung eines
    E-Mail-Accounts beantragt. Um Eure E-Mail-Adresse zu nutzen werdet Ihr eine einfache Frage auswählen müssen, die es Euch erlaubt, bei
    vergessenem oder verlorenem Passwort den Zugang wiederherzustellen. Solltet Ihr auch die Frage und deren Antwort vergessen, werden wir
    nicht mehr in der Lage sein, den Zugang zu Eurem Account wiederherzustellen. Das Einzige, was uns dann noch bleibt, ist, Euch einen
    neuen E-Mail-Account zur Verfügung zu stellen; die Daten Eures alten Accounts sind jedoch verloren.  
     In diesem Fall solltet Ihr in Eurem neuen Zugang die "recover question" einstellen, indem Ihr dem entsprechenden Link in Eurem
    Account-Kontrollzentrum folgt. Seid auch daran erinnert, dass periodisch das Passwort gewechselt werden sollte: das geht über den Link
    "change password" im Kontrollzentrum.
- **Empfang von Spam und Viren**: Der Server nutzt zwar Antivirus- und Antispam-Software, aber wie jeder x-beliebige kommerzielle Host
    auch, ist die Effektivität solcher Programme gering. Jeder Nachricht wird ein gewisser Spam-Rang zugewiesen, der verbreiteten
    Richtlinien zu Spamming folgt. Wenn der Rang hoch genug ist, wird die Betreffzeile der Nachricht mit dem Wort \*\*\* SPAM
    \*\*\* markiert. Ihr werdet dann das E-Mail-Programm entsprechend konfigurieren können, um solche Spam-Nachrichten herauszufiltern. Wir
    empfehlen trotzdem, die als Spam markierten Nachrichten zu überprüfen, da Antispam-Filter alles andere als perfekt arbeiten.
- **Versenden von Spam und Viren**: Um zu vermeiden, dass unsere Server in den Blacklists anderer Server auf der halben Welt auftauchen,
    wollen wir nicht, dass über unsere E-Mail-Accounts Spam verschickt wird. Jeder Account, der dabei erwischt wird, wird ohne jede
    Nachricht darüber sofort deaktiviert.
- **Rechtliche Verantwortlichkeiten**: Wie auch für alles andere, was Ihr tut, müsst Ihr Euch im Klaren sein, dass unsere Server nicht
    verantwortlich sind für das, was Ihr schreibt oder über Euch preisgebt. Wir empfehlen daher, [sämtliche verfügbaren
    Werkzeuge](/docs/mail/privacymail) zu nutzen, um Euer Recht auf Privatsphäre zu verteidigen.

--------------------------------------------------------------------------------------------------------------------------------------------

### <a name="web"></a>Web Hosting und Blogs

- Die Verantwortlichkeit für den Inhalt von Webseiten liegt bei den Website-Administratoren (webmaster). Das Kollektiv, dass den Server
    Autistici-Inventati betreibt, kann nicht für die Inhalte gehosteter Webseiten verantwortlich gemacht werden.  
     Wir weisen deshalb alle Webmaster darauf hin, unsere Prinzipien zu achten und nur Speicherplatz zu erfragen, wenn unser
    [Manifest](/who/manifesto) gelesen und diesem zugestimmt wird.
- Der Speicherplatz für Webseiten wird nur mit dem Einverständnis des Kollektives erteilt. Persönliche Webseiten werden nicht gehostet, es
    sei denn, sie veröffentlichen Dokumente allgemeinen Interesses, die mit unserem [Manifest](/who/manifesto) und den Meinungen des
    Kollektivs im Einklang stehen.
- Wir erinnern daran, dass der Upload von urheberechtlich geschütztem Material (wie etwa mp3 oder divx) nicht gestattet ist, da der
    Vorgang die Existenz unserer Server und aller daran hängenden Dienste stark gefährdet.
- Wenn Ihr an Eurer Seite arbeitet, denkt auch an den Kampf um den freien Austausch von Wissen, den wir kämpfen. Wir hoffen, dass ihr
    gegenüber diesen Kämpfen sensibel seid. Also denkt an uns, wenn Ihr am unteren Ende Eurer Seite die kleine, aber bedeutende
    "Copyright"-Warnung seht.  Das Material, welches auf diesem Server veröffentlicht wird, soll mindestens eine **freie Lizenz** haben
    (z.B.: [GPL](http://www.gnu.org) oder [Creative Commons](http://www.creativecommons.org)) -- solltet Ihr Euch überhaupt dazu
    entscheiden, eine Copyright-Lizenz zu nutzen ;)
- Die Grenzen Eures Webspeicherplatzes werden von den Kapaziäten der Maschine begrenzt. Das soll bedeuten, dass der Platz nicht für
    persönliche Daten und sehr große Dateien genutzt wird, soweit es nicht unbedingt notwendig ist. **Generell vertrauen wir auf
    Zusammenarbeit und Verantwortung**.
     Müsst Ihr **sehr große Dateien** on-line verfügbar haben, dann kontaktiert uns vor dem Upload, um die Verfügbarkeit des Speicherplatzes
    zu überprüfen.
- Unsere Server sollen die Privatsphäre und die Anonymität unserer Nutzer\_innen sicherstellen: darum ist es nicht erlaubt, Besucherzähler
    oder andere Dienste für Webstatistiken (wie shinystat oder google analytics) auf einer Webseite zu betreiben. Solche Dienste speichern
    die IP-Adressen der Nutzer\_innen und vergehen sich damit an einem unserer Hauptprinzipien. Solltet Ihr wirklich wissen wollen, wie
    viele Leute Eure Webseite besuchen, nutzt unseren eigenen neuen [Dienst](/services/piwik) für Webstatistik.
- Wenn Ihr an Eurer Webseite arbeitet, denkt daran, dass nicht jede/r Zugang zu den neuesten Technologien hat und dass es auch blinde
    Menschen gibt; für unser Kollektiv ist die **Zugänglichkeit** der gehosteten Seiten sehr wichtig.
- In der [Propaganda-Abteilung](/who/propaganda "propaganda") unserer Webseite findet Ihr ein kleines Logo des
    autistici.org/inventati.org-Projektes und wir würden uns sehr freuen, wenn die Seiten, die von uns gehostet werden, dieses Logo irgendwo
    auf der Homepage darstellen lassen ;))))).
     Zudem: solltet Ihr Eure eigene Domain nutzen ([technische Hinweise](/docs/web/domains) lesen), würde die Einbindung eines
    kleinen Inventati/Autistici-Logos als kleines, aber wichtiges Zeichen verstanden, dass Ihr unser Projekt teilt und unterstützt.
- **Es ist nicht erlaubt, den zugeteilten Platz als eine Umleitung (re-direct) für andere Webseiten zu nutzen**, da wir das als nutzlose
    Verschwendung unserer Ressourcen betrachten.
- Server-Speicherplatz, der mehr als 90 Tage nach der Aktivierung ungenutzt bleibt, wird deaktiviert und gelöscht (aus den gleichen
    Gründen wie oben).
- Wenn die Nutzung der mysql-Datenbank beantragt wurde, geschieht die Wartung Eurer Datenbank über das Web-Interface.
     ES IST NICHT NOTWENDIG irgendwelche Software wie phpadmin oder ähnliche Web-Interfaces zu installieren, um Euren MySQL-Account zu
    verwalten, da wir das bereits installiert haben und uns gut darum kümmern.

- - -

Solltet Ihr weitere Fragen oder Zweifel haben, schaut in unsere [FAQ](/docs/faq/) oder [kontaktiert uns!](mailto:info@autistici.org)


