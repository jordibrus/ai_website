title: Policy
----

Policy
======

To be hosted on our servers you have to share our principles of **anti-fascism,
anti-racism, anti-sexism, anti-homophobia, anti-transphobia, and
anti-militarism**. Your projects must as well be based on the same
**non-commercial nature** which keep our project alive, and on the desire to
share and experience relationships and struggles, with all the patience it
requires ;))))

Any service provided on our servers cannot be destined to (directly or
indirectly) commercial or religious activities, nor to political parties: to
make a long story short, we do not host anyone who already has means and
resources to spread widely his/her own contents and ideas, or who uses the
concept of (explicit or implicit) delegation and representation in its
day-to-day relationships and projects.

The server only keeps the logs that are strictly necessary for debug operations,
and they do not hold any personal data connecting accounts on our servers to
actual identities. To read more about how we respect your data, read [this document](/who/your_data).

Dos and Donts for Specific services:
------------------------------------

- [E-Mail (as well as Mailing lists and Newsletters)](#mail)
- [Web Hosting and Blogs](#web)

----------------------------------------------------------------------------------------------------------

### E-Mail dos and donts (as well as MLs and NLs for the issues concerning spam)

<a name="mail"></a>

To open an account on our server means also to share [our
manifesto](/who/manifesto) and to take note of the following:

- **Space limits on disk:** The use of your account is limited by the respect
  owed to everybody who's using our services. Resources are scarce, the service
  is free and the users are many. For this reason, we invite you to limit the
  use you make of our disk by downloading or deleting your emails as often as
  possible.
- **Usage/Non-usage of this service:** Mailboxes which are left unused for over
  365 days will be disabled. If you think you won't be using your account for
  more than 1 year but would like to keep it active anyway, [write to
  us](mailto:info@autistici.org).
- **WebMail usage:** The WebMail needs a lot of the servers' resources, and
  should be therefore only used for emergencies; for normal usage you are
  invited to use an email client and download your emails on your PC.
- **Password:** For privacy-related reasons, we never keep track of anybody who
  requests the activation of an email account. To use your email, you will have
  to choose a simple question which will allow you to recover your password in
  case you lose/forget it. If you also forget the question or the answer to the
  question we won't be able to re-send you the data to access your account
  again. The only thing we will be able to do is creating a new mail account for
  you, but you won't recover your old stuff.  Remember then to set the "recover
  question" in your account clicking on the link you will find in your user
  panel. Remember as well to periodically change your password: you can do it by
  clicking on the "change password" link in the user panel.
- **Receiving Spam and Viruses:** The server uses antivirus and antispam tools,
  but as with any other commercial host, they are little effective. Every mail
  is awarded a spam rank using some widespread guidelines about spamming. If the
  mail rank is high enough it is tagged with the word \*\*\* SPAM \*\*\* in the
  Subject line of the mail. You will then be able to configure your mail client
  to filter any spam message. We recommend you to check the emails which are
  "tagged" as spam, since the anti-spam filter is anything but perfect.
- **Sending Spam and Viruses:** To avoid having our servers included in
  blacklists of half the known world, we don't want our email accounts be used
  for sending spam. Any account caught doing so will be disabled without further
  notice.
- **Legal responsibilities:** As for anything else you do, be aware that our
  servers are not responsible for what you write, nor for the safeguard of your
  own privacy. We therefore invite you to make the most of all the existing
  [tools](/docs/mail/privacymail "protect your privacy") to defend your right to
  privacy.

- - -

### Web Hosting and Blogs

<a name="web"></a>

- Responsibility for the content of the sites lies within the sites' webmasters.
  The collective which runs the server Autistici-Inventati cannot be held
  responsible for the contents of any of the hosted sites.  We therefore invite
  all webmasters to respect our principles and to request the webspace only
  after having read and agreed to our [manifesto](/who/manifesto).
- The webspace will be provided according only to the collective approval.
  Personal websites will not be hosted unless they publish documents of
  particular interest according to our [manifesto](/who/manifesto) and to the
  collective's opinions.
- We remind you not to upload on the site copyright protected materials (like
  sometimes mp3 and divx are) that could endanger the existence of our very
  servers (and all services connected to it).
- When you work on your site, have a thought about the struggle for free sharing
  of knowledge that we are fighting. We hope you'll be sensitive to these
  struggles. Think about us when you see the small but significant "copyright"
  warning down on the bottom of your page.  The material published on this
  server will have to be released at least under **a free license** (like for
  example: [GPL](http://wwww.gnu.org/), or [Creative
  Commons](http://www.creativecommons.org)) -- if you decide to use a copyright
  license at all ;)
- The limits to your webspace are limited by the machine's resources. This means
  that it's not good to use space for personal data or for big-sized files,
  unless it's strictly necessary. **Generally, we confide in collaboration and
  responsibility**.  **If you do have very big files** to keep online, we invite
  you to contact us to verify the disk space availability before you upload.
- Our servers mean to safeguard the privacy and anonymity of its users: it's
  therefore not allowed to use counters or other webstat services (like
  shinystat or google analytics) on any website page, since they log the IPs of
  people visiting the site, completely forsaking one of our main principles. If
  you really want to know how many people visit your site, feel free to use our
  [web statistics service](/services/piwik "A/I Piwik statistics").
- Doxing (i.e. the publication of other people's personal data) is not allowed
  because we cannot possibly establish the good faith with which this
  information was published or the consequences for the people who are subject
  to doxing, and also because it exposes us to too high legal risks. We have
  decided to ban doxing also considering that for this kind of activity there
  are many services better than ours (even better if they're hidden services).
- When you work on your site, think about the fact that not everyone has access
  to the latest existing technologies and that there are also people who cannot
  see; for us **accessibility** of the hosted sites on this server is always
  important (see <http://www.ecn.org/xs2web> for further information on the
  issue of accessibility).
- In the [propaganda](/who/propaganda "propaganda") page of our site you'll find
  a little logo of the autistici.org/inventati.org project, and we'd be very
  happy to know that the sites hosted by our servers display it somewhere in
  their homepage ;))))). Moreover, in case you are using your own domain (read
  [technical notes](/docs/web/domains "Register your own domain")), the
  inclusion of a little Inventanti/Autistici-logo on your home page is
  considered a little but important sign that you share and support our project.
- **It is not allowed to use the assigned space as a re-direct for other
  sites**, as we consider this an unnecessary waste of our resources.
- Server space left unused for more than 90 days from the activation will be
  disabled and deleted (for the same reasons as above).
- If the use of mysql has been requested, the maintenance of your own database
  happens through the web interface.  IT IS NOT NECESSARY to install any
  software like phpadmin or similar web interfaces to manage your MySQL account,
  because we have already installed them and keep them well tended and cared
  for.

- - -

If you have further doubts have a look at our [FAQ](/docs/faq/) or [contact
us](mailto:info@autistici.org)!

- - -

<small>Thanks to [(A)iMoNDi](http://www.videoteppista.nomasters.org/) who
contributed this translation.</a> </small>


