<html>
<head>
  <title>Rassegna stampa/web</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>E' arrivato un vagone carico di...</h3>
  <small>Documentazione sulla citazione da parte di Trenitalia contro Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="news.php">Aggiornamenti</a></li>
   <li><a href="mirrors.php">Mirrors</a></li>
   <li><a href="legaldocs.php">Documentazione legale</a></li>
   <li><a href="rassegna.php">Rassegna stampa/web</a></li>
   <li>Comunicati:
	<ul>
	<li><a href="comunicato.html">Primo Comunicato A/I</a></li>
	<li><a href="comunicato_ricorso.php">Secondo Comunicato A/I</a></li>
	<li><a href="comunicato_zenmai23.php">Comunicato Zenmai23</a></li>
	</ul></li>
<li><a href="link.php">Link su Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.php">Cosa posso fare?</a></li>
	<ol>
	<li><a href="protesta.php#mail">Manda una mail a Trenitalia</a></li>
	<li><a href="protesta.php#banner">Pubblica il banner sul tuo sito</a></li>
	<li>Firma il <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.php#signature">Usa questa signature!</a></li>
	<li><a href="protesta.php#stampa">Stampa e diffondi il materiale</a></li>
	<li>Fai una <a href="http://autistici.org/it/donate.html">donazione!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page del progetto Autistici/Inventati</a><br/>

 </div>


 <div id="main">



<h2>Rassegna stampa/web</h2>

<p>
Aggiornamenti nella sezione Diritti Digitali su
<a href="http://italy.indymedia.org/feature_xs.php#471">
Italy Indymedia
</a>
</p>


<ul>

 <li>
  <a href="rassegna/espresso36.jpg">Effetto Boomerang</a>
  - L'espresso 
 </li>

 <li>
  <a href="http://punto-informatico.it/p.asp?i=49297">Trenitalia, voglia di censura</a>
  - Punto Informatico 
    <a href="rassegna/pi_230804.html">[copia cache]</a>
 </li>

<li>
	<a href="http://dicorinto.it/archives/000267.html">Una censura ad alta velocit�</a> - Il Manifesto (02 Settembre 2004) <a href="rassegna/manifesto_040903.html">[Copia chache]</a>
</li>
<li>
<a href="rassegna/corriere20040906.png">Rettifica di Trenitalia</a> - Il Corrirere della Sera</li>




 <li>
  <a href="http://zeusnews.it/index.php3?ar=stampa&cod=3332&numero=999">A Trenitalia non piace la satira on-line</a>
  - ZeusNews
    <a href="rassegna/zeusnews_130804.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://italy.peacelink.org/cybercultura/articles/art_6454.html">Trenitalia contro Autistici</a>
  - PeaceLink
    <a href="rassegna/peacelink_120804.html">[copia cache]</a>
 </li>


 <li>
  <a href="http://wup.it/article.php?sid=6368">La morte viagga comoda</a>
  - Wup.it
    <a href="rassegna/wup.html">[copia cache]</a>
 </li>

 </li>

 <li>
  <a href="http://www.autistici.org/vcm/archivio.epl?dir=2004-09-10">Notturna vcm su trenitalia vs. autistici [mp3]</a>
  - Va a ciapa i mouse, trasmissione su Radio Popolare di Milano
 </li>

 </li>


 <li>
  <a href="http://www.socialpress.it/breve.php3?id_breve=375">A Trenitalia le critiche non piacciono</a>
  - Socialpress.it
    <a href="rassegna/socialpress_it.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://ilgrandeboh.zapto.org/index.php?p=1036">Pare che trenitalia...</a>
  - IlGrandeBoh!
    <a href="rassegna/ilgrandeboh_130804.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.kuht.it/modules/news/article.php?item_id=1139">Censura</a>
  - Kuht.it
    <a href="rassegna/kuht_it.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://fsa.anarchismus.org/index.php?id=419">�tok na projekt Autistici.org</a>
  - fsa.anarchismus.org [cz]
    <a href="rassegna/fsa.anarchismus_cz.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.eff.org/minilinks/archives/week_2004_08_22.php#001850">Italian Protest Site Censored</a>
  - EFF minilinks
    <a href="rassegna/eff_week_2004_08_22.html#001851">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.ilmanifesto.it/Quotidiano-archivio/28-Agosto-2004/art107.html">Il Manifesto</a>
  - Un vagone di denuncie per i mediattivisti
    <a href="rassegna/ilmanifesto.html">[copia cache]</a>
 </li>


 <li>
  <a href="http://www.fabiopani.it/?module=blog&timeid=040830183626">Voglia di censura (una volta si bruciavano i libri)</a>
  - Fabiux
    <a href="rassegna/Fabiuxblog_it.htm">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.ilsecolodellarete.it/html/modules.php?op=modload&name=News&file=article&sid=40&mode=thread&order=0&thold=0">La censura di Trenitalia contro la libert� di satira in rete</a>
  - Il Secolo della Rete
    <a href="rassegna/Il_Secolo_della_Rete.htm">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.acidlife.com/thisworld/archives/000063.php#more">Trenitalia contro Autistici</a>
  - acidlife
    <a href="rassegna/acidlife_com.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.altroadige.it/News/view_content?news_id=20040829164208">Comunicato Autistici/Inventati: Trenitalia contro Autistici</a>
  - Altroadige
    <a href="rassegna/altroadige_it.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.anarcotico.net/modules.php?op=modload&name=News&file=article&sid=1028">Trenitalia cita in giudizio Autistici/Inventati</a>
  - Anarcotico
    <a href="rassegna/anarcotico_net.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://bellaciao.org/fr/article.php3?id_article=9155">Un serveur alternatif italien contraint de fermer un site Web � la demande de la compagnie Trenitalia</a>
  - Bellaciao [fr]
    <a href="rassegna/bellaciao_org.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://blog.buonricordo.org/archives/000058.html">Trenitalia vs Autistici</a>
  - buonricordo.org
    <a href="rassegna/blog_buonricordo_org.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://brainstream.splinder.com/archive/2004-08">brainstream</a>
  - brainstream
    <a href="rassegna/brainstream.htm">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.cut-up.net/cms/index.php?option=news&task=viewarticle&sid=222">Sotto attacco</a>
  - Cut up
    <a href="rassegna/cut-up_net.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://hns-info.net/article.php3?id_article=4375">Un h�bergeur alternatif italien oblig� de fermer un site</a>
  - hns-info.net
    <a href="rassegna/hns-info_net.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://navigando.blogspot.com/2004/08/trenitalia-e-censura.html">Trenitalia e censura</a>
  - Navigando
    <a href="rassegna/navigando_blogspot_com.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.photorail.com/forum/topic.asp?TOPIC_ID=3506">Trenitalia contro il web?</a>
  - Photorail.com
    <a href="rassegna/photorail_com.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.rebelion.org/noticia.php?id=3917">Italia: censurada p�gina contra Trenitalia</a>
  - Rebelion
    <a href="rassegna/rebelion_org.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://infos.samizdat.net/blog/page.php?p=1215">Un serveur alternatif italien contraint de fermer un site Web � la demande de la compagnie Trenitalia</a>
  - Samizdat
    <a href="rassegna/samizdat_net.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://acp.sindominio.net/article.pl?sid=04/08/12/1527233&mode=thread&threshold=0">Censura a Autistici/Inventati</a>
  - Sindominio [es]
    <a href="rassegna/sindominio.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.welcomepalermo.com/dblog/articolo.asp?id=330">La morte viaggia (anche) in trenitalia</a>
  - Welcomepalermo
    <a href="rassegna/welcomepalermo_com.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.znort.org/modules/news/article.php?storyid=788">TRENITALIA VS AUTISTICI</a>
  - Znort
    <a href="rassegna/znort_com.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.zmag.org/Italy/autistici-trenitalia.htm">� arrivato un vagone carico di...</a>
  - Zmag
    <a href="rassegna/zmag_autistici-trenitalia.htm">[copia cache]</a>
 </li>

 <li>
  <a href="http://www.thething.it/modules.php?name=News&file=article&sid=155">La riscossa degli zerbini</a>
  - The Thing
    <a href="rassegna/thething.it.html">[copia cache]</a>
 </li>

 <li>
  <a href="http://punto-informatico.it/p.asp?i=49467">Trenitalia contro certi mirror</a>
  - Punto Informatico
    <a href="rassegna/pi_060904.html">[copia cache]</a>
 </li>

<li>
  <a href="http://www.indignato.it/archive/019044.html">Zenzur Train</a>
  - L'Indignato
    <a href="rassegna/lindignato.html">[copia cache]</a>
 </li>


<!--
 <li>
  <a href="LINK">TITOLO ARTICOLO</a>
  - SITO
    <a href="rassegna/">[copia cache]</a>
 </li>
-->

</ul>


</body>
</html>

<hr/>

<p>
<a href="comunicato_zenmai23.php">
Comunicato del disciolto Zenmai23
</a>
</p>


  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>
