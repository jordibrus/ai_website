title: Qui som
----

Qui som
=======

A/I va nàixer fa més de 10 anys quan individus i col·lectius ocupats/des en qüestions de tecnologia, privacitat, drets digitals i activisme
polític es van reunir a Itàlia.  La nostra idea bàsica és proveir una ampla varietat de ferramentes de comunicació lliures, impulsant a les
persones a triar aquelles opcions lliures en lloc de les comercials.  Volem informar i formar al voltant de la necessitat de protegir la
nostra pròpia privacitat amb la fi d'evitar el saqueig que governs i corporacions porten a terme de manera indiscriminada amb les nostres
dades i personalitats.

Tractem d'aconseguir açò oferint serveis a internet (espais web, correus electrònics, llistes de correu, chat, missatgeria instantània,
reenviament de correus anònims, blocs, newsletters, i més) tant a persones com a col·lectius i projectes que comparteixen les nostres idees,
a l'hora que fem el millor que sabem per defendre la privacitat del@s nostres usari@s.

Ens mantenim fora del circuit i actitud comercial dels serveis webs de pagament, som feliços d'acollir a tot@s les agitador@s que lluiten
infatigablement contra la censura mediàtica i cultural, i contra la globalització d'un imaginari preconfeccionat i empaquetat, que
diàriament passa per ser l'únic possible.

Per conéixer més sobre nosaltres i els nostres objectius:

* La nostra [política](/who/policy)
* [privacy policy](/who/privacy-policy)
* Una breu [història](/who/telltale) del col·lectiu
* El [manifest](/who/manifesto) que escrivim en 2002 per explicar qui som i que volem
* [Propaganda, flyers, bàners i gràfiques](/who/propaganda)
* El [Pla R\*](/who/rplan)
* Els [costos](/who/costs) de la infraestructura

Els nostres servidors estan legalment a càrrec d'[Associazione AI-ODV](mailto:associazione@ai-odv.org). 
For technical problems, write to [helpdesk](mailto:help@autistici.org)
Qualsevol qüestió d'un altre tipus, ha de dirigir-se cap al nostre correu electrònic de contacte: info at investici.org.


