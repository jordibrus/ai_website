title: +KAOS - Il libro sui 10 anni di A/I
----

+KAOS - Il libro sui 10 anni di A/I
===================================

10 anni di mediattivismo e hacking
----------------------------------

A 10 anni dalla nascita del nostro [collettivo](/who/collective) siamo riusciti a raccontare almeno una parte di quello che abbiamo
fatto e di quello che ha significato per noi (e forse non solo per noi). Quella che segue è la quarta di copertina che riassume i suoi
contenuti.

![+KAOS Book Cover Low Resolution](/static/img/book/ai-book-cover-web.jpg)

"L’esperienza collettiva di un gruppo di ragazze e ragazzi
appassionati di tecnologia e comunicazione che hanno fatto proprio il motto di Primo Moroni *“Condividere saperi, senza fondare poteri”.*

*“All’inizio la lista era un casino”* – Pinke

*“Era il 2000, no, no, no, era il 2001, aspetta guardiamo... ah sì (sospiro). Era maggio”* – Cojote.

    [iptables -A INPUT -p all -s ! 127.0.0.1 -j DROP]

Alla fine del XX secolo la scena hacker era avanguardia pura. Quando le idee, le pratiche e le scorribande nella rete di questa nicchia di
sperimentatori telematici iniziarono ad attirare l’attenzione del mainstream, in Italia un manipolo di attivisti ebbe l’intuizione che la
comunicazione fosse davvero la sostanza in cui si sarebbero espressi i processi sociali, politici e culturali dell’immediato futuro.

Il collettivo A/I, o Autistici/Inventati, nasce nel 2001 con l’obiettivo di creare un server autogestito e fornire gratuitamente servizi web
nel rispetto dell’anonimato e della privacy. Il loro veicolo informatico è sopravvissuto a molti tentativi di repressione, a denunce,
sequestri, inchieste giudiziarie. Nel tempo, ha costruito una rete di server collocati in molti paesi del mondo che gli permette di offrire
a diverse migliaia di utenti gli strumenti per una navigazione consapevole, che tutela la loro libertà di informazione e comunicazione.

Questo libro è prima di tutto un azzardo, un tentativo di narrazione pensato a partire dai ricordi di chi in A/I c’è stato, di chi passava
di lì per caso ed è rimasto, di chi ha dato una mano, di chi ancora, ogni giorno decide che ne vale la pena. È, al contempo, il racconto di
un’avventura abbastanza unica nel mondo del digitale e la ricostruzione di una serie di percorsi formativi mai lineari, al limite tra gioco
e impegno politico.

Presentazione di Sandrone Dazieri

Prefaziosa di Ferry Byte

Curato da:

Laura Beritelli (Firenze, 1978) ha una laurea in Ermeneutica Filosofica. Dal 2007 è redattrice della rivista Humana.mente, un
quadrimestrale di studi filosofici pubblicato gratuitamente online.

Materiali
---------

- Copertina del libro: [low-res](/static/img/book/ai-book-cover-low.jpg) - [hi res](/static/img/book/ai-book-cover-hi.jpg)
- [La prefaziosa di Ferry Byte](http://www.agenziax.it/?pid=63&estratto=1&sid=30)
- La versione in PDF del libro: [Scaricatevela!](/static/img/book/ai-book-kaos.pdf)
- La versione in ePub del libro: [Scaricatevela!](/static/img/book/ai-book-kaos.epub)
- La versione in mobi del libro: [Scaricatevela!](/static/img/book/ai-book-kaos.mobi)

Mentre scaricate il nostro libro, ricordatevi che [A/I sopravvive grazie alle vostre donazioni](/donate)! Mettetevi una mano sul
cuore!

Prezzo consigliato negli infoshop: 10 euro

Prezzo in libreria: 14 euro


