title: SSL: un tunel criptado para proporcionar seguridad a nuestras comunicaciones  
----

# SSL: un túnel criptado para proporcionar seguridad a nuestras comunicaciones 

Cuando os estáis conectando a una pagina web a través de **https** y cuando habéis
configurado vuestro cliente de correo u el programa que usáis para chatear para
que usen el cifrado SSL, vuestro ordenador recibe un certificado SSL desde el
servidor de A/I, de manera que se pueda crear un túnel cifrado para proteger
vuestras comunicaciones.

Por muchos años A/I ha elegido de firmar sus certificado SSL sin recurrir a una
autoridad de certificación comercial, porqué esto hubiese comportado meter la
relación de confianza entre nosotros y nuestros usuario en las manos de sujetos
de los cuales no podemos confiar: estados, fuerzas "del orden", empresas cuyo
único objetivo es el beneficio.

En diciembre 2015 hubo el lanzamiento de [Let's
Encrypt](https://letsencrypt.org/), una autoridad de certificación (fundada,
entre otros, por la [Electronic Frontier Foundation](https://www.eff.org/)) que
proporciona certificados SSL gratuitos y simples de instalar con el objetivo
declarado de agilizar y difundir este tipo de criptografia.
Por esta razón ahora todos los servicios de A/I están cifrado con llaves SSL
firmadas por Let's Encrypt, que vuestros navegadores y vuestros clientes
reconocen de manera automática (si habéis abierto esta pagina en HTTPS en vez
que HTTP, podéis observar un candado de color verde en la barra de dirección,
que indica que la conexión es segura).

Esto significa que no tenéis que hacer nada especial para conectaros de manera
segura con nuestros servidores, y que deberéis comprobar que vuestros clientes
sean configurados para no aceptar certificados SSL no validos (que a veces era
necesario hacer antes, por ejemplo en Xchat).

El único servicio que no está usando un certificado de Let's Encrypt, sino uno
comercial, es [Noblogs](https://noblogs.org/): su estructura nos obligaría a
crear un certificado para cada blog, lo cual sería un malgasto de los recursos
de Let's Encrypt. La solución es usar un certificado wildcard (que permite
crear conexiones seguras con todos los dominios \*.noblogs.org), pero estos
certificados no están (todavía) proporcionados por Let's Encrypt. 
