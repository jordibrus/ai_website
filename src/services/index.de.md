title: Angebotene Dienste
----

Angebotene Dienste
==================

A/I stellt Werkzeuge zur anonymen Kommunikation und zum anonymen Austauschen von Inhalten bereit. Wir raten dazu, unsere Dienste zusammen
mit einigen [Werkzeugen zum Schutz der Privatsphäre](/docs/mail/privacymail) zu verwenden.

Bei allen unseren Diensten legen wir besonderen Wert auf den Schutz der Privatsphäre unserer Nutzer. Aus diesem Grund protokollieren wir
**keine Verbindungsdaten und ebensowenig Informationen über Dienste, Identitäten oder Namen von Nutzer/innen**. Außerdem unterstützen wir
ausdrücklich den Gebrauch von Verschlüsselungswerkzeugen, die wir unseren Nutzern anbieten (behalten Sie das im Hinterkopf, bis Sie Ihr
Mailprogramm oder Ihre Website einrichten!).

Um die Dienste, die unser Netzwerk anbietet, nutzen zu können, müssen Sie die Grundprinzipien des **Antifaschismus, Antirassismus,
Antisexismus, Antimilitarismus und den nicht-kommerziellen Ansatz** mit uns teilen, worauf dieses Projekt gegründet ist. Genauso wichtig ist
uns die Wertschätzung von menschlichen Beziehungen und das Miteinander-Teilen im Gegensatz zu Heuchelei und Kommerz. Aber wenn Sie es auf
unserer Website bis hierher geschafft haben, sollte das kein Problem sein ;)))!

Nachdem Sie all das gelesen haben, können Sie [hier](/get_service) einen Dienst beantragen.

## Welche Dienste bietet A/I an?

Wir bieten eine Reihe verschiedenster Kommunikationswerkzeuge an:

- [Blogs](/services/blog "Blog bei Noblogs.org") / [Web-Hosting](/services/website "Website bei A/I")
- [Anonymisierungsdienste](/services/anon "Anonymisierungsdienste bei A/I") / [Persönliche VPNs](https://vpn.autistici.org/ "Persönliches A/I VPN")
- [E-Mail-Konten](/services/mail "E-Mail-Konto bei A/I") / [Mailinglistem, Newsletter](/services/lists "Mailingliste bei A/I")
- [Instant-Messaging und Chat](/services/chat "Instant-Messaging und Chat bei A/I")

## Kosten

Die Dienste, die wir anbieten, haben keinen bestimmten Preis, aber wie Sie sich leicht vorstellen können, ist der Betrieb des gesamten
Netzwerks recht teuer (derzeit sind es etwa 13.000 Euro pro Jahr).

Wenn Sie wollen, dass unsere auch weiterhin verfügbar bleiben und unser Projekt weiterbesteht, ist es daher nötig, dass alle Personen,
Gruppe Projekte, die unsere Dienste in Anspruch nehmen, unserem Netzwerk eine jährliche Spende übermitteln, je nach deren jeweiligen
Möglichkeiten.

Die Höhe der Spende kann variieren von wenigen Euros für eine Mailbox, über 25-30 Euro für eine Standard-Website, bis hin zu höheren
Beträgen, wenn Sie eine Gruppe oder ein Kollektiv sind (hier bieten sich zum Beispiel Fundraising-Partys an). Vielleicht sind Sie aber auch
einfach jemand, dem es möglich ist und der willens ist, bedeutsamere Summen beizutragen.

In keinem Fall kontrollieren wir Ihre wirtschaftliche Aktivität, setzen aber etwas Verständnis und guten Willen voraus. Denn falls wir kein
Geld bekommen, sind wir gezwungen, den Betrieb einzustellen.

Denken Sie darüber nach, und [spenden](/donate) Sie bitte.
