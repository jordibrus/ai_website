title: Servicio de anonimato
----

# Servicio de anonimato

En esta sección proponemos nuestro servicio pensado específicamente para mantener el anonimato de nuestr\*s usuari\*s.

Probablemente, si has entrado en esta parte de nuestra web es que ya has reflexionado sobre lo que aprecias tu anonimato en la red, pero
como estas cosas es mejor repetirlas, vale la pena hacer hincapié en porqué recomendamos encarecidamente salvaguardar el anonimato de todas
formas posibles.

Por ejemplo, hay quienes piensan que resulta excesivo para ciertos servicios, es de paranoic\*s pensar que alguien nos controle mientras
navegamos por la red o cuando escribimos un correo electrónico. De hecho, las intrusiones son mucho más comunes de lo que la gente piensa
habitualmente, y l\*s que piensan que "no tienen nada que ocultar" no estarían tan content\*s al saber que sus correos electrónicos enviados
sin cifrar pueden ser leídos por curios\*s de todo tipo y que los datos de su navegación por Internet son interesantes para un sector del
mercado que hace negocio con ellos.

Por eso te invitamos a informarte sobre nuestro servicio de anonimato y a leer nuestro [blog](https://cavallette.noblogs.org), en el que
encontrarás novedades y programas para la defensa de la privacidad y conecerás nuevas formas de acabar con nuestro anonimato.

También puedes consultar los manuales que escribimos acerca de la privacidad y el anonimato de los servicios en nuestra
[sección especial de howto](/docs/anon/).

Remailer Anónimo
----------------

Un remailer anonimo es básicamente un modo de esconder nuestra identidad cuando enviamos un mensaje electrónico. Un mensaje enviado a través
de un remailer anónimo no se puede atribuir a su autor, debido a esto, tampoco se puede recibir respuesta.

Para mayor información puedes consultar [la página web de nuestro remailer](https://remailer.paranoici.org) y nuestro
[manual del remailer](/docs/anon/remailer).

Nym Server
----------

Un nymserver ofrece una identidad bajo un pseudónimo de larga duración. Está basado en una red de servidores anónimos de remailer que
permiten la creación de direcciones de correo con el envío y la recepción de forma anónima.

Para más información puedes consultar [la página web de nuestro remailer](https://remailer.paranoici.org/nym.php).
