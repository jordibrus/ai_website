title: Noblogs: blog senza log
----

Noblogs: blog senza log
==============================

Il [progetto NoBlogs](http://noblogs.org) nasce dalla volontà di aggiornare l'offerta dei servizi di [A/I](http://autistici.org)
all'evoluzione che gli strumenti della comunicazione digitale hanno avuto negli ultimi anni. Ci siamo resi conto che un numero consistente
di persone non hanno bisogno di un vero e proprio sito web, ma in primo luogo di **uno spazio in cui comunicare con immediatezza e
semplicità**.

Potete usare il vostro blog come vi pare, a patto di **rimanere all’interno della nostra** [policy generale](/who/policy) e della
specifica [policy di Noblogs](http://noblogs.org/policy/ "NoBlogs Policy").

In realtà noi speriamo che questo servizio venga usato non tanto per scrivere il proprio diario (anche, ci mancherebbe), ma soprattutto per
produrre contenuti che contribuiscano a uno spazio di **informazione, comunicazione, relazione e iniziativa politica indipendente**.

**La piena libertà d’espressione richiede una piena attenzione alla privacy e alla sicurezza**. Noi facciamo la nostra parte offrendo
servizi basati su software libero, evitando di mantenere log e di richiedervi dati personali, fornendovi alcuni link utili in particolar
modo quando si parla di blog. **Ma sta a voi fare la vostra parte**, leggendo le [guide che vi consigliamo](/docs/blog/) e
prendendo ogni precauzione possibile. Ve ne ricordiamo alcune.

- Smetti di usare per ogni comunicazione importante gmail, hotmail e tutti i fornitori commerciali di mailbox, tanto più che **caselle di
  posta di questo tipo non verranno accettate per [l’apertura di un blog](http://noblogs.org/register)**.
  Utilizza piuttosto il nostro servizio di mailbox o quello di un altro server autogestito dalla policy analoga alla nostra 
    (un elenco si trova [qui](/links)).
- Ricorda che di ogni comunicazione elettronica non crittata viene mantenuta copia su innumerevoli altri server sui quali non abbiamo
    alcun controllo.
- Non utilizzare programmi che non siano open source, poiché non è possibile conoscerne il funzionamento, né scoprire eventuali funzioni
    nascoste che violino la privacy.
- Presta la massima attenzione quando comunichi il tuo nome e cognome, numeri di telefono, indirizzi e qualsiasi altra
    informazione sensibilei.
- I software di analisi statistica delle visite mettono in pericolo la privacy delle persone che visitano il vostro blog, per questo
    abbiamo messo a vostra disposizione il nostro [software di statistiche web](/services/piwik "Matomo web analitics for A/I"):
    certo è meno dettagliato di google analitics, ma protegge la privacy di chi visita il vostro sito.

**Ora se vuoi aprire un blog da noi puoi andare su [Noblogs.org](https://noblogs.org/register/) e farlo!**

**Attenzione!!!! Informazione di servizio riguardante la sicurezza dei server di A/I!** Se quello di cui avete bisogno è solo pubblicare
contenuti e notizie, vi raccomandiamo caldamente di aprire un blog su Noblogs invece di chiederci un sito su cui installare voi stessi
software come Wordpress o simili. Questo perché il software **va regolarmente aggiornato** per motivi di sicurezza, e se usate Noblogs non
dovrete farlo voi, penseremo a tutto noi :)
