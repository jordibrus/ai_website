title: Noblogs: blogs sans logs
----

Noblogs: blogs sans logs
================

[Le projet NoBlogs](http://noblogs.org/) viens du désire de garder [l'offre de services d'A/I](/services/) à jour avec les
outils de communication qui sont apparu ces dernière années. Nous avons porté notre attention sur le fait que la plupart des internautes
n'ont pas vraiment besoin d'un site web mais plutôt d'un **espace de communication simple et directe**.

Vous pouvez blogger le contenu que vous souhaitez **à condition que vous respectiez la** [politique générale](/who/policy) de
nos serveurs et la [politique spécifique](http://noblogs.org/policy/) de notre plateforme de blog.

Nous espèrons que ce service ne soit pas utilisé pour tenir un journal (vous pouvez l'utiliser ainsi bien sur, si vous en avez envie) mais
pour générer un contenu interessant favorisant la production d'une **information indépendante et collective, la communication, la création
d'un réseau de relation ou la mise en place d'un espace d'initiative politique**.

**La liberté d'expression implique d'étre particulièrement attentif à la vie privé et à la sécurité**. Nous faisons de notre mieux en
utilisant uniquement des logiciels libre, en ne gardant aucun log ou données personnelles, ainsi qu'en suggérant des lien utiles parlant de
blogs et de protection de la vie privé... Mais ce ne sera pas suffisant si **vous ne faites pas de votre mieu** en
[lisant les guides que nous vous suggérons](/docs/blog/) ainsi qu'en prenant autant de précautions que possible pour assurer votre sécurité. Nous
sommes heureux de rappeler certaines d'entre elles à vous tous.

- Arrêtez d'utiliser gmail, hotmail ou tout autre fournisseur d'e-mail commercial pour échanger des informations importantes et sensibles.
    **Une adresse venant d'un de ces fournisseurs ne sera pas acceptée [pour enregistrer un nouveau blog](http://noblogs.org/register)**. 
      Utilisez notre service de mail ou tout autre service autonome et non-commercial ([regardez ici !](/links)).
- Souvenez-vous que beaucoup de serveurs partout sur internet gardent une copie de toute communication électronique non cryptée.
- N'utilisez pas de logiciel au code source fermé, vous ne pouvez pas savoir comment ils fonctionnent et si ils possèdent une quelconque
    fonction pouvant compromettre votre vie privée.
- Faites très attention lorsque vous envoyez par voie electronique des noms, prénoms, numéro de téléphone, adresse et tout autre
    donnée personnelle.
- Les logiciels d'analyse mettent en danger la vie privée des personnes surfant sur votre blog, c'est pourquoi nous utilisons notre propre
    [outil de statistiques](/services/piwik): oui c'est moins "informatif" que google analytics, mais il garde vos visiteurs à
    l'abris des regards indiscrets.

Maintenant vous pouvez aller sur [Noblogs.org](https://noblogs.org/register/) et ouvrir votre blog

Très important concernant la sécurité du serveur. Si vous souhaitez simplement publier du contenu, nous vous recommandons d'opter
pour un blog sur Noblogs plutôt que pour un site web avec un logiciel type Wordpress. Ceci car ces logiciels **doivent** être régulièrement
mis à jour, alors que c'est inutile avec Noblogs :)
