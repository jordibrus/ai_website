title: Quienes somos
----

Quienes somos
=============

A/I nació hace más de 10 años cuando individuos (personas) y colectivos ocupad\*s en cuestiones de tecnología,
privacidad, derechos digitales y activismo político se reunieron en Italia. Nuestra idea básica es proveer una amplia variedad de
herramientas de comunicación libres, impulsando a las personas a elegir aquellas opciones libres en lugar de las comerciales. Queremos
informar y formar sobre la necesidad de proteger nuestra propia privacidad con el fin de evitar el saqueo que gobiernos y corporaciones
llevan a cabo de forma indiscriminada sobre nuestros datos y personalidades.

Tratamos de conseguir ésto ofreciendo servicios en internet (sitios web, correo electrónico, listas de correo, chat, mensajería instantánea,
re-envío de email anónimo, blogs, newsletters, y más) tanto a personas como colectivos y proyectos que comparten nuestras ideas, a la vez
que hacemos lo que mejor que sabemos para defender la privacidad de nuestr\*s usuari\*s.

Manteniéndonos fuera del circuito y actitud comercial de los servicios web de pago, somos felices de acoger a tod\*s l\*s agitador\*s que
luchan infatigablemente contra la censura mediática y cultural, y contra la globalización de un imaginario preconfeccionado y empaquetado,
que pasa a diario por ser el único posible.

Para saber más sobre nosotr\*s y nuestros objetivos:

* Nuestra [política](/who/policy)
* Nuestra [privacy policy](/who/privacy-policy)
* El [manifesto](/who/manifesto) que escribimos en 2002 para explicar quienes somos y que queremos.
* Una breve [historia del colectivo](/who/collective)
* [Propaganda, flyers, banners y gráficas](/who/propaganda)
* El [Plan R\*](/who/rplan)
* Los [costos](/who/costs) de la infraestructura.

Nuestros servidores están legalmente a cargo de [Associazione AI-ODV](mailto:associazione@ai-odv.org). 
Si encuentra problemas de tipo tecnico, [helpdesk](mailto:help@autistici.org)
Cualquier cuestión de otro tipo, debe dirigirse hacia nuestro correo electrónico de contact: info at investici.org.
