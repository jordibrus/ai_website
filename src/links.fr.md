title: Les Collectifs Radical Tech
----

Les Collectifs Radical Tech
---------------------------

### Serveurs anonymes et indépendants offrant des adresses et autres services avec une politique similaire à la nôtre

- [ecn.org](http://www.ecn.org) (ECN est un espace de relations et de contacts entre personnes ayant été dispersées par les changements de notre société actuelle, entre des individus qui ne renonceront pas à l’actuelle idéologie d’uniformité et de marginalité, entre des sujets souhaitant créer un réel mouvement, capable de changer l’état actuel des choses - Italie).
- [indivia.net](http://indivia.net) (listes d’adresses, torrents audio, hébergement web, IRC, e-mails, forums, noms de domaine avec un serveur indépendant vert dont le nom signifie « salade d’endive » - Italie)
- [oziosi.org](http://www.oziosi.org) (Oziosi.org reconnaît l’état naturel de paresse, de lenteur et d’oisiveté auquel des femmes et hommes libres peuvent tranquillement désirer aspirer - Italie)
- [free.de](http://www.free.de) (Un projet historique allemand offrant de nombreuses adresses, listes d’adresses et hébergements web pour activistes – Allemagne)
- [resist.ca](http://resist.ca) (Le Collectif Resist! est formé d’un groupe d’activistes de Vancouver travaillant afin de fournir des services techniques et de communication, ainsi que d’informer et d’éduquer la plus grande communauté d’activistes. Le projet Collectif Resist! (Resist!) et resist.ca provient du vieux Collectif TAO de Vancouver – Canada)
- [riseup.net](http://riseup.net) (riseup.net fournit des adresses, des listes, et des hébergements web à des personnes mettant leurs efforts pour un changement social libératoire. Un projet dans le but de créer des alternatives démocratiques et des pratiques d’auto-détermination à travers le contrôle sécurisé de moyens de communication – USA)i
- [squat.net](http://squat.net) (squat!net est une revue internationale sur internet avec pour thème principal les maisons squattées, offrant des services internet dans des espaces libres – Pays-Bas)
- [so36.net](http://so36.net) (serveur indépendant – Allemagne)
- [nadir.org](http://nadir.org) (Un projet pour une révision des principes de Gauche, à travers la création d’un espace de communication et d’information – Allemagne)
- [aktivix.org](https://aktivix.org/) (independent server offering email, mailing lists and VPN)
- [boum.org](http://boum.org) (Serveur indépendant et anonyme – collectif basé en France)
- [shelter.is](https://shelter.is) (tech collective providing solidarity driven privacy friendly services)
- [espiv.net](https://espiv.net) (Cybrigade is an autonomous collective with main point of report, social fights and how these are expressed in cyberspace, cybrigade is managing espiv.net's services. Providing e-mail accounts, mailing lists, blogs - greece)


### An ste incomplète de projets auxquels nous sommes reliés…

- [freaknet.org](http://www.freaknet.org) (Un laboratoire expérimentant les technologies de communication – Catane, Sicile – Italie)
- [interactivist.net](http://interactivist.net/) (Un effort collaboratif, une source de communication activiste, un projet d’indépendance médiatique et un projet sur le partage d’habilité en technologie – USA)
- [nodo50.org](http://nodo50.org) (Un projet autonome pour l’information télématique – Espagne)
- [tao.ca](http://tao.ca) (tao.ca est un projet anarchiste d’un petit collectif de la région de Toronto nommé OAT, qui s’est développé en TAO Communications à Toronto et qui est affilié à d’autres projets tech, tels que resist.ca et interactivist.net – Canada)

