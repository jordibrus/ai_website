title: Mail for OSX Configuration Howto
----

Mail for OSX Configuration Howto
====================================

**Apple Mail**, also known as **mail.app** or simply **Mail**, is the full featured mail client which comes with OS X.

It is pretty and easy to use, but we don't recommend it because it is not Free Software. However, it is more stable than
[Thunderbird](/docs/mail/thunderbird), so for many it is the best choice when using OS X.

Before you configure Apple Mail, you need to [decide if you will be using IMAP or
POP](/docs/mail/connectionparms). Apple Mail has **very** good IMAP support, so we suggest that most
people use IMAP.

-   [Apple Mail tutorial for POP](#pop)
-   [Apple Mail tutorial for IMAP](#imap)

If you would like to use GPG with Apple Mail follow the instructions:

1.  Download the latest version of [GNU Privacy Guard](http://macgpg.sourceforge.net/) for your version of MacOS.
2.  Mount the GnuPG Mac OS X disk image and run the .mpkg installer.
3.  Download the latest version of [GPGKeys](http://macgpg.sourceforge.net/).
4.  Decompress the .tar.gz and drag the GPGKeys file to your Applications folder.
5.  Run GPGKeys. From the "Key" menu, select "Generate". A Terminal window will open. You can select the default options for each choice
    unless you know what you're doing. When prompted, enter your real name, email address, and comment (handle). Once the key is generated
    you can quit Terminal.
6.  Download the [Sen:te PGP plug-in for Mail](http://www.sente.ch/software/GPGMail/English.lproj/GPGMail.html).
7.  Run the "Install GPGMail" script.
8.  Open Mail. Everything should be working now. When composing a new message you'll see a new checkbox that lets you choose your key and
    sign your message. When receiving a signed message you'll be able to verify it.

<a name="pop"></a>

How to configure Mail for OSX to download mail from A/I via POP protocol (with SSL support)
-------------------------------------------------------------------------------------------

**Note:** screenshot for the POP protocol are in Italian but we hope you'll be able to glimpse how to configure your Mail.app software
anyway.

1.  Open Mail.app and click on *Mail* on the Menu bar. Then choose the *Preferences* tab.
2.  In the *Account* window click on the *+* sign below.
3.  In the popup window fill in your [connection parameters](/docs/mail/connectionparms) as shown in the picture below
4.  Click on *Server Settings* to configure the outgoing mail server.
5.  Enter the *Advanced* tab and choose *SSL*
6.  Confirm the choice and Mail will be ready to download your messages from A/I servers via Pop/SSL

<a name="imap"></a>

How to configure Mail for OSX to download mail from A/I via IMAP protocol (with SSL support)
--------------------------------------------------------------------------------------------

<a name="first_time_user"></a>

### First Time User

*If you are already using mail, see [Existing User](#existing_user) below.*

The first time you start mail, a dialog will appear requesting your account information:
![](/static/img/man_mail/en/welcomeimap.gif)

1.  Enter your name.
2.  Enter your full email address, i.e. joe\_hill<!-- -->@<!-- -->autistici.org
3.  Incoming and SMTP servers should be "mail.autistici.org" and "smtp.autistici.org" respectively.
4.  Select "IMAP" account type
5.  Your username coincides with your email address, in this case, "joe\_hill@autistici.org".
6.  **VERY IMPORTANT!** If you plan to set up secure connections, do <u>not</u> enter your correct password, because it will be transmitted
    over the internet in "clear text". Enter an <u>incorrect</u> password here. You will occasionally get a dialog box requesting your
    password when mail attempts to check your mail. Ignore it until you finish setting up the secure connections. Just hit cancel when these
    messages appear.
7.  Select "OK".

An error message will appear, but just click continue: ![](/static/img/man_mail/en/loginerror.gif)

<a name="existing_user"></a>

### Existing User

*This section is for adding an account profile if you have already been running Apple Mail. Users who just configured an account in the
[First Time User](#first_time_user) section should skip this.*

![](/static/img/man_mail/en/accountinfoimap.gif)

1.  Select menu item *Mail* &gt; *Preferences...*
2.  Select the *Accounts* tab.
3.  Click on the *+* button in the lower-left corner of the screen.
4.  For account type, select *IMAP*.
5.  Enter a description (it can be anything, we suggest your email address).
6.  Enter your full email address, i.e. joe\_hill<!-- -->@<!-- -->autistici.org.
7.  Your incoming mail server is mail.autistici.org
8.  Your user name is your A/I address.

<a name="managing_account_settings"></a>

### Managing Account Settings

![](/static/img/man_mail/en/preferences.gif)

In this section, we assume you have the *preferences window* open. To do so:

1.  Select menu item *Mail* &gt; *Preferences...*

<a name="setup_encryption_for_receiving_mail"></a>

#### Setup Encryption For Receiving Mail

1.  Click on *Accounts* and select your A/I email account
2.  Now select the *Advanced* tab.
3.  Click "Automatically sychronize changed mailboxes"
4.  Click "Use SSL"

![](/static/img/man_mail/en/imapadvancedsettings.gif)

<a name="setup_encryption_for_sending_mail"></a>

#### Setup Encryption For Sending Mail

![](/static/img/man_mail/en/serversettingsclickimap.gif)
_ Click On "Account Information"
- Make sure that your SMTP server is set to smtp.autistici.org
 *Note for existing mail users: You will need to select "Add Server" from the SMTP pull-down menu.*
- If it is, select "Server Settings"
- For *Server Port*, put 25 or 465. We recommend using port 465, because many ISPs block port 25.
- Select *Use Secure Socket Layer (SSL)*.
- Set *Authentication* to *Password*
- Now, enter your correct user name and your *correct* password.
- Select *OK*.

![](/static/img/man_mail/en/smtp.gif)

<a name="complete_the_setup"></a>

### Complete the Setup!

![](/static/img/man_mail/en/accountinfoimap.gif)

Now, simply enter you **correct** password in the password box. Your password will be encrypted when sent to A/I server to send and receive
email.

We wish to thank [riseup](http://www.riseup.net) for the inspiration for [these howto](http://help.riseup.net/mail/mail-clients/apple-mail/).
