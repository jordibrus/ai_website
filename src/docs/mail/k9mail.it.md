title: Configurazione di K-9 Mail
----

Configurazione di K-9 Mail
========================

[**K-9 Mail**](https://github.com/k9mail/k-9/wiki) è un client di posta Free Software per Android. Fornisce varie funzionalità in più
rispetto al client di default di Android, tra cui il supporto per APG, la versione di GPG per Android. Potete trovare il manuale di K-9 Mail
[qui](https://github.com/k9mail/k-9/wiki/Manual).

Installazione di K-9 Mail
-------------------------

Se non avete già installato K-9 Mail, la maniera più semplice per farlo è attraverso Google Play o F-Droid.

1.  Assicuratevi che la connessione ad Internet (via 3G, Wifi, o condivisione USB) del vostro cellulare/tablet/periferica Android sia attiva
    e funzionante.
2.  Aprite **F-Droid**, repository di software libero e open-source (se non l'avete andate [qui](https://f-droid.org/)!), e cercate "K-9
    Mail", o in alternativa visitate [questo indirizzo](http://f-droid.org/repository/browse/?fdfilter=k-9&fdid=com.fsck.k9). Se volete, il
    programma potete trovarlo anche su [Google Play Store.](https://play.google.com/store/apps/details?id=com.fsck.k9)
3.  Cliccate il pulsante "Installa" e attendete che l'applicazione venga scaricata sulla vostra periferica Android e installata.

Se preferite l'installazione manuale, è possibile scaricare il pacchetto .apk direttamente dal repository di K-9 Mail a
[questo indirizzo](https://code.google.com/p/k9mail/downloads/list).

Come configurare K-9 Mail per leggere la posta di A/I via IMAP
--------------------------------------------------------------

In questo manuale ci occuperemo di configurare il nostro account di posta con il protocollo [IMAP](https://it.wikipedia.org/wiki/IMAP). La
configurazione via [POP](https://it.wikipedia.org/wiki/POP) è leggermente distinta, ma abbastanza semplice se sostituite ai [parametri di
configurazione](/docs/mail/connectionparms) i valori per la connessione POP.

1.  Aprite K-9 Mail. Se non avete già configurato un account, vi verrà mostrato automaticamente il menù di configurazione di un
    nuovo account. Altrimenti, premete il tasto "Menu" del vostro telefono, e selezionate "Aggiungi Account".

     ![](/static/img/man_mail/it/k9mail-howto-1.jpg)

2.  Inserite il vostro indirizzo di posta su A/I completo di dominio (per esempio, *utente@anche.no*).

     ![](/static/img/man_mail/it/k9mail-howto-2.jpg)

3.  Selezionate *IMAP* come tipo di account.

     ![](/static/img/man_mail/it/k9mail-howto-3.jpg)

4.  La procedura di configurazione vi chiederà di [parametri di connessione](/docs/mail/connectionparms) del
    vostro account. Inserite il vostro indirizzo di posta completo come nome utente e potete lasciare tutte le altre configurazioni con i 
    valori di default. In fondo alla pagina potrete infine configurare, in base al tipo di connessione internet presente (Wifi, 3G, etc.), 
    quando scaricare la posta (utile, per esempio, se volete scaricare la posta solo quando siete connessi via Wifi, 
    per non consumare banda sulla connessione 3G).

     ![](/static/img/man_mail/it/k9mail-howto-4.jpg)

5.  Vi verrà ora mostrata la schermata dove inserire i parametri di configurazione per la posta in uscita. Inserite smtp.autistici.org 
    come server SMTP e il resto va lasciato invariato.

     ![](/static/img/man_mail/it/k9mail-howto-5.jpg)

6.  Potrete ora configurare il comportamento di K-9 Mail come più vi aggrada, a seconda che preferite essere sempre aggiornati sulle nuove
    email o se preferite risparmiare banda e batteria.

     ![](/static/img/man_mail/it/k9mail-howto-6-jpg)

7.  Nella ultima schermata, inserite il vostro nickname ed eventualmente un nome per la configurazione che avete appena concluso.

     ![](/static/img/man_mail/it/k9mail-howto-7.jpg)

8.  Complimenti, avete configurato K-9 Mail con il vostro account di A/I!

     ![](/static/img/man_mail/it/k9mail-howto-8.jpg)

Purtroppo al momento K-9 Mail non permette di configurare un account senza memorizzare la password.

**Questo è un grave rischio per sicurezza del vostro account, nel caso che perdiate il vostro telefono, vi venga sottratto o finisca nelle
mani sbagliate.**

Inoltre la password che memorizza viene conservata in chiaro. Ciò significa che chiunque venga in possesso del telefono può vederla (con un
accesso di root, la password è solo trasformata base64). Quando il telefono è spento e se usate la
[criptazione del dispositivo android](http://support.google.com/android/bin/answer.py?hl=en&answer=1663755) la password può comunque essere più al sicuro.

Per maggiori informazioni sui problemi di sicurezza di K-9 Mail andate
[qui](http://grokbase.com/p/gg/k-9-mail/128ae88rjg/k-9-password-security).

Per una possibile soluzione al problema andate [qui](http://code.google.com/p/k9mail/issues/detail?id=1243).
