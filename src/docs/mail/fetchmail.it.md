title: Configurazione di Fetchmail
----

Configurazione di Fetchmail
====================================

    Updated: 2016-04-06
    Keywords: fetchmail, SSL, Autistici, Inventati
    Description: come usare fetchmail con supporto ssl con il server Autistici.
    Language: it (Italian)
    Compatibility: fetchmail (SSL enabled) 

Questo micro-HOWTO spiega come configurare fetchmail per scaricare la posta dal server di A/I utilizzando il protocollo SSL e verificando
che stia dialogando davvero con il server A/I

Il file di configurazione di fetchmail *~/.fetchmailrc*, quindi aggiungere a quel file le seguenti righe (e se non esiste crearlo):

    # inizio configurazione autistici
    poll "mail.autistici.org"
    proto imap
    user "utente@dominio.org"
    pass "password"
    ssl
    sslcertck
    sslcertpath "/etc/ssl/certs/"
    folder INBOX
    folder Spam
    nokeep
    # fine configurazione autistici


**ATTENZIONE**: per verificare i certificati dovete avere installato il pacchetto ca-certificates


    Sostituite "utente@dominio" con il vostro indirizzo di posta elettronica,
    "password" con la relativa password


    Nota che la riga "password" è più sicuro
    ometterla ed inserire ogni volta la password a mano da tastiera,
    altrimenti si rischia che chiunque riesca a scoprire la password del
    proprio account locale, o quella di root, e chiunque abbia la
    possibilità di mettere le mani sull'hard disk possa venire in possesso
    della password dell'account di posta.


    L'opzione ssl fa sì che la comunicazione avvenga attraverso il
    protocollo cifrato SSL, mentre l'opzione sslfingerprint fa sì che
    il software verifichi la corrispondenza tra le fingerprint ssl specificate
    nell'opzione e quelle fornite dal server di A/I, in modo da evitare che qualcuno
    si intrometta tra voi e A/I per rubarvi dati e password.



    I comandi folder servono a indicare quali cartelle scaricare (INBOX e Spam
    vanno bene di default), nokeep specifica che volete scaricare i
    messaggi e cancellarli dal server. Leggete la pagina di manuale di fetchmail (1) per maggiori informazioni
    ('man 1 fetchmail').
