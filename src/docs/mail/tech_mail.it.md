title: Note tecniche sulle caselle di posta
----

Note tecniche sulle caselle di posta
===========================================

Il sistema di posta di A/I si presenta all'utente in modo completamente decentralizzato: per leggere e inviare la posta è possibile
connettersi ad uno qualsiasi dei server che appartengono alla rete. Questo è lo scopo degli indirizzi speciali `mail.autistici.org` e
`smtp.autistici.org`, che permettono di "smistare" le connessioni tra i vari server in modo più o meno casuale.

Secondo quanto previsto dal [Piano R\*](/who/rplan/), la tua casella di posta si trova materialmente in un dato momento su uno
solo dei server della rete di A/I.

Per questo è possibile a volte che un problema temporaneo di connettività renda la tua mailbox inaccessibile: quando questo si verifica, la
posta destinata a te **non** va assolutamente persa, ma rimane in coda sugli altri server della rete.

Nel caso che il problema non sia di natura effimera, il tuo account di posta verrà spostato su un server differente, e potrai riprendere a
leggere la nuova posta (mentre per quella vecchia bisognerà attendere il ripristino del server che si è rotto).

Parametri di connessione
------------------------

I parametri necessari per connettersi ai server di posta ti dovrebbero essere stati inviati con la mail di attivazione della casella. In
ogni caso sono brevemente ricapitolati in [questa pagina](/docs/mail/connectionparms)

Invio della posta
-----------------

Ultimamente abbiamo avuto segnalazioni di problemi con l'invio della posta attraverso svariati fornitori di connettività (Telecom, ad
esempio). Ciò è principalmente dovuto al fatto che sempre più providers decidono di bloccare le connessioni in uscita dalle utente
domestiche sulla porta 25, in un goffo tentativo di ridurre l'invio di spam.

Ricordiamo che, dove è possibile, le porte raccomandate per la connessione per l'invio della posta in uscita sono:

- **465**, con SSL
- **587**, con TLS

Ci preme ricordarvi anche che da una casella di posta non è possibile mandare grandi quantità di mail a un numero elevato di indirizzi: se
avete bisogno di un servizio del genere, chiedeteci di aprirvi una newsletter!

Filtri anti-spam
----------------

Autistici/Inventati implementa sui propri server alcuni meccanismi pensati per ridurre la ricezione di email spazzatura (*spam*). Dato che
non ci è possibile configurare nel dettaglio questi filtri per ciascun utente, essi sono in genere molto conservativi (cioè lasciano passare
più messaggi possibile).

I messaggi contrassegnati come spam vengono salvati in una cartella "Spam" che viene ripulita automaticamente ogni 30 giorni di tutto il
suo contenuto.

Se consultate la vostra e-mail via via **webmail** o via **IMAP** potrete valutare la correttezza della presenza di un messaggio nella
cartella "Spam" direttamente.

Viceversa se un utente consulta la propria posta solo attraverso un client e il protocollo **POP** non potrà visualizzare la cartella
"Spam" e il suo eventuale contenuto. Questo aumenta il rischio che messaggi erroneamente catalogati come SPAM vengano cancellati perché
abbandonati nella cartella "Spam".

Per evitare questo spiacevole inconveniente ci sono solo due soluzioni: consultare di tanto in tanto via **webmail** la vostra casella di
posta e risistemare adeguatamente il contenuto della cartella "Spam"; [configurare un filtro](/docs/mail/sieve) dalla vostra
**webmail** che sposti tutti i messaggi della cartella "Spam" nella vostra "Posta in Arrivo" da cui poi potrete valutare autonomamente la
rilevanza dei messaggi.

Come conseguenza di alcuni dei sistemi antispam utilizzati, è possibile che a volte la consegna dei messaggi sia ritardata di qualche minuto
(ma è comunque bene ricordare che la posta elettronica su Internet non garantisce tempi di recapito istantanei: è bene averlo presente prima
di preoccuparsi di un'eventuale disfunzione dei servizi di posta di A/I)...
