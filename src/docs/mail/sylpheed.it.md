title: Come configurare Sylpheed
----

Come configurare Sylpheed
==================

    Version: 0.1.2
    Updated: 15-09-2005
    Keywords: Slackware 10.1,sylpheed 1.04
    Description: Usare Sylpheed per inviare email via autistici con SSL
    Language: it (Italian)

Questo mini HOWTO contiene informazioni su come configurare il client MUA sylpheed per comunicare in SSL con lo smarthost di
mail.autistici.org.

Dal menu configurazione selezionate *Crea nuovo account*, dopodichè dovete impostare [qualche parametro](/docs/mail/connectionparms)
come indicato nelle figure che seguono, ovviamente sostituendo a "tuonome" il vostro
indirizzo email completo.

![](/static/img/man_mail/it/posta_sylpheed1.jpg)

![](/static/img/man_mail/it/posta_sylpheed2.jpg)

![](/static/img/man_mail/it/posta_sylpheed3.jpg)

![](/static/img/man_mail/it/posta_sylpheed4.jpg)
