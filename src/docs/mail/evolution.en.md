title: Evolution Configuration Howto
----

Evolution Configuration Howto
=================================

-   To create an account click on the *Edit-&gt;Preferences* menu.
-   Click on *Account* and choose *Add*.
-   In the *Identity* window fill in your nickname and your full e-mail address. Then choose *Next*
-   In the *Receiving Email* window choose the Protocol (POP or IMAP), in the *Configuration-&gt;Server* field fill in "mail.autistici.org",
    in the *Username* field fill in your full e-mail address. 
    in the security section, set "STARTTLS after connecting" as encryption method, then choose *Next*

![](/static/img/man_mail/en/Evolution-1.jpg)

-   Personalize your *Receiving Options* then choose *Next*
-   In the *Sending Email* window choose "SMTP" as *Server Type*, in the *Configuration-&gt;Server* fill in "smtp.autistici.org", mark the
    checkbox *Server requires authentication* and the *STARTTLS after connecting* as encryption method. 
    In the *Authentication* tab choose "PLAIN" and fill in your full email address.

![](/static/img/man_mail/en/Evolution-2.jpg)

-   Choose *Next* until the end.


A/I wishes to thank Radiodurito and Michael English for this manual.


