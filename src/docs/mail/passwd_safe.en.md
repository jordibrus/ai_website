title: How to create a safe password
----

How to create a safe password
=============================

Because passwords are almost always the weakest link in any security system where they are used, the first step to better security is better password practice.

Things to avoid:

*	**Don’t pick your birth date, a dictionary word or a proper noun!**. Passwords are often easy to crack because most people pick a password that is a variation on a word in the dictionary. There are simply not that many words in human languages: it is trivial for a computer to try them all! This includes words where you have replaced some letters with numbers. For example, “L0V3” is just as easy to crack as “LOVE”.

*	**Don’t use the same password for all your accounts**. Also, it can be better to write down your passwords in a secure place rather than use the same one everywhere.

*	**Don’t forget to change your password**. You should change your password at least once a year.

*	**Never tell anyone your password**, especially if they ask for it.

How do you create a password that is strong and yet easy to remember? This can be really tough. There are three generally approved methods:

**1. Use a password storage locker**

Don’t try to remember passwords. Instead, generate random passwords for all the different services and websites you use, and store them in a secure password locker. We suggest [KeepassX](https://www.keepassx.org/): it is open source and multi-platform (it is available for Linux, Mac and windows).


**2. Passwords**

a. Start with **multiple words** you can easily remember.

b. Convert these words to **non-words** (for example, by taking the first letter of each word).

c. Add a few random uppercase letters, numbers, or symbols, and you are done.

For example:

You could turn _“The Revolution Will Not Be Televised”_ into _"trwNbt"_ and then add a few random characters for _“trwNbt!42”_.


**3. Passphrases**

a. Pick a few random words you can easily remember, for instance _autistici_ + _crypto_ + _resist_ + _paranoia_. Mixing in words from different languages and non-dictionary words is a good idea.

b. String these together into a long passphrase: _"autisticicryptoresistparanoia"_. This will be longer, but easier to type.

c. In order to further increase the complexity of the password you can add words which are not present in ant dictionary (like _"autisticicryptoresistparanoia7grln"_).

d. Do not choose passphrase/password longer than 60 characters, since it could cause troubles reading the mail.

Here is another example:

![](/static/img/man_mail/it/passwd_safe.png)
