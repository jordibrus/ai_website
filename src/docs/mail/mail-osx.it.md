title: Configurazione di Mail.app per OSX
----

Configurazione di Mail.app per OSX
============================

**Mail.app** o **Apple Mail** è il client di posta standard di OS X

E' facile da usare e fa tutto quello che un programma di posta dovrebbe saper fare, ma non è tra quelli che noi raccomandiamo di usare
perché non è Free Software. E' però più stabile di [Thunderbird](/docs/mail/thunderbird) quindi potrebbe essere la scelta
migliore se lavorate in ambiente Mac.

Prima di configurare Mail.app dovrete però [decidere se userete il protocollo POP o il protocollo IMAP](/docs/mail/connectionparms).
Mail.app ha un ottimo supporto per il protocollo IMAP, quindi suggeriamo alla
maggior parte di voi di sfruttarlo.

- [Apple Mail tutorial for POP](#pop)
- [Apple Mail tutorial for IMAP](#imap)

Uso di GPG con Mail.app
-----------------------

Se pensate di voler usare GPG con **Mail.app** seguite le seguenti istruzioni

1.  Scaricate l'ultima versione di [GPG per Mac](http://macgpg.sourceforge.net/)
2.  Montate l'immagine del disco di GnuPG per Mac OS X e lanciate l'installatore .mpkg
3.  Scaricate l'ultima versione delle [GPGKeys](http://macgpg.sourceforge.net/)
4.  Decomprimete il .tar.gz e trascinate il file di GPGKeys nella vostra cartella Applicazioni
5.  Lanciate GPGKeys. Dal menù *Key* selezionate *Generate*. Un terminale si aprirà e potrete selezionare le opzioni predefinite per ognuna
    delle opzioni a meno che sappiate quello che state facendo e dobbiate fare degli aggiustamenti. Quando vi verrà chiesto inserite il nome
    che volete venga visualizzato come mittente, il vostro indirizzo mail e un commento. Una volta che la chiave è stata generata potete
    chiudere il terminale
6.  Scaricate il plugin [Sen:te PGP per Mail](http://www.sente.ch/software/GPGMail/English.lproj/GPGMail.html).
7.  Lanciate lo script *Install GPGMail*
8.  Aprite la vostra posta e tutto dovrebbe funzionare correttamente. Quando state scrivendo un nuovo messaggio vedrete una casella che vi
    permetterà di selezionare quale chiave usare per crittare e firmare il messaggio che state scrivendo. Quando vi arriveranno messaggi
    crittati sarete in grado di decrittarli immediatamente e verificare il mittente.

<a name="pop"></a>

Come configurare Mail.app per scaricare la posta di A/I via POP
---------------------------------------------------------------

1.  Apri Mail.app e clicca su *Mail* sulla barra del menu, quindi seleziona *Preferenze*. ![](/static/img/man_mail/it/posta_mail_mac_1.png)
2.  Nella finestra *Account* clicca sul simbolo del *+* che trovi in basso. ![](/static/img/man_mail/it/posta_mail_mac_2.png)
3.  Quando la finestra si apre, inserisci i dati come nell'immagine. ![](/static/img/man_mail/it/posta_mail_mac_3.png)
4.  A questo punto clicca su *Impostazioni Server* per configurare il server in uscita. ![](/static/img/man_mail/it/posta_mail_mac_4.png)
5.  Adesso entra nella scheda *Avanzate* e seleziona SSL ![](/static/img/man_mail/it/posta_mail_mac_5.png)
6.  A questo punto conferma la tua scelta e Mail e' configurato per essere utilizzato con autistici/inventati!

<a name="imap">

Come configurare Mail.app per scaricare la posta di A/I via IMAP
----------------------------------------------------------------

**N.B.**: le immagini presenti in questa sezione del manuale sono solo in inglese. Se avete tempo e modo di farne delle versioni in italiano
da inviarci saremo felici di migliorare questo manuale con il vostro contributo.

<a name="first_time_user"></a>

### Utenti che usano Mail per la prima volta

*Se avete già usato Mail allora passate alla sezione per gli [Utenti già esistenti](#existing_user).*

La prima volta che lanciate il programma di posta, vi comparirà una finestra che vi chiederà alcune informazioni per configurare il primo
account di posta sul PC.
![](/static/img/man_mail/en/welcomeimap.gif)

1.  Inserite il nome che volete venga visualizzato come mittente.
2.  Inserite il vostro indirizzo di posta intero (es. joe\_hill<!-- -->@<!-- -->autistici.org
3.  I server per scaricare la posta e il server SMTP per inviarla dovrebbero essere rispettivamente "mail.autistici.org" e
    "smtp.autistici.org"
4.  Scegliete di configurare un utente "IMAP" per il vostro programma
5.  Il vostro utente coincide con il vostro indirizzo di posta elettronica, ad es: "joe\_hill@autistici.org".
6.  **MOLTO IMPORTANTE!** Non inserite qui la vostra vera password, altrimenti verrà inviata per effettuare il primo test di connessione
    **in chiaro**. Inserite una password <u>sbagliata</u>, correggerete questo errore in seguito quando avrete configurato il supporto SSL
    per connettervi su un canale crittato al server di A/I.
7.  Cliccate su *Ok*

Vi apparirà un messaggio di errore, ma ignoratelo e cliccate su *continua* ![](/static/img/man_mail/en/loginerror.gif)

<a name="existing_user"></a>

### Utenti che hanno già usato Mail.app

*Questa sezione è pensata per chi deve aggiungere un profilo di posta di A/I a quelli che ha già configurato in Apple Mail. Se avete
configurato Mail.app come descritto nella precedente[sezione dedicata ai nuovi utenti Mail.app](#first_time_user) potete saltare i
prossimi passaggi.*

![](/static/img/man_mail/en/accountinfoimap.gif)

1.  Selezionate il menu *Mail* &gt; *Preferences...*
2.  Selezionate il tab *Accounts*.
3.  Cliccate sul segno *+* nell'angolo in basso a destra dello schermo
4.  Selezionate dai tipi di account da creare, *IMAP*.
5.  Inserite una descrizione (potete mettere quello che vi pare, ma noi suggeriamo di metterci il vostro indirizzo mail)
6.  Inserite il vostro indirizzo di posta completo, ad es: joe\_hill<!-- -->@<!-- -->autistici.org.
7.  Inserite come server per la posta in ingresso mail.autistici.org
8.  Inserite come nome utente il vostro indirizzo di posta A/I

<a name="managing_account_settings"></a>

### Modificare le impostazioni del vostro account di posta

![](/static/img/man_mail/en/preferences.gif)

In questa sezione presumiamo siate entrati nella *finestra Preferences* di Mail.app. Per farlo:

1.  Selezionate il menu *Mail* &gt; *Preferences...*

<a name="setup_encryption_for_receiving_mail"></a>

#### Scaricare la posta via SSL

1.  Cliccate su *Accounts* e selezionate il vostro indirizzo di posta A/I/
2.  3.  Selezionate il tab *Advanced*.
4.  Cliccate su "Automatically sychronize changed mailboxes"
5.  Cliccate su *Usa SSL*

![](/static/img/man_mail/en/imapadvancedsettings.gif)

<a name="setup_encryption_for_sending_mail"></a>

#### Inviare posta via SSL

![](/static/img/man_mail/en/serversettingsclickimap.gif)

1.  Cliccate su *Account Information*
2.  Assicuratevi che il vostro server SMTP sia "smtp.autistici.org"
     *<u>Nota per gli utenti che hanno già configurato Mail.app per altri accounti</u>: dovrete aggiungerlo con "Add server"*
3.  Se il server SMTP è quello giusto, cliccate su *Server Settings*
4.  Come *Server Port* mettete 25 o 465. Raccomandiamo di usare la 465 dato che molti provider bloccano la 25 (a causa degli spammer).
5.  Selezionate la voce *Use Secure Socket Layer (SSL)*.
6.  Impostate la voce *Authentication* su *Password*
7.  Ora inserite il vostro *nome utente* e la vostra *password <u>corretta</u>*
8.  Cliccate su *OK*.

![](/static/img/man_mail/en/smtp.gif)

<a name="complete_the_setup"></a>

### Ora completiamo l'opera!

![](/static/img/man_mail/en/accountinfoimap.gif)

Ora non dovrete far altro che inserire la password **corretta** nella finestra dove ve lo richiede. La vostra password sarà crittata e
inviata ai server di A/I per inviare e per scaricare la posta.

Ringraziamo sentitamente [riseup](http://www.riseup.net) per l'ispirazione nello scrivere buona parte di questo
[manuale](http://help.riseup.net/mail/mail-clients/apple-mail/).
