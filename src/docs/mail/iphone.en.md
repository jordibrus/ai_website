title: iPhone Configuration Howto
----

iPhone Configuration Howto
==============================

(includes SSL)
--------------

This howto explains how to set up a mail account with A/I so that your messages are encrypted while travelling through the web and it is
more difficult to tap them.

In the following example we will set up the account of "joe", corresponding to the address "joe@autistici.org" and to the user name "joe".

**Note:** This setup has been tested on IOS 7, but there should be only slight differences from other versions. Go to, Settings &gt; Mail,
Contacts, Calendars:

![](/static/img/man_mail/en/mail-iphone1.jpg)

Select "Add Account"

![](/static/img/man_mail/en/mail-iphone2.jpg)

Select "Other"

![](/static/img/man_mail/en/mail-iphone3.jpg)

Select "Add Mail Account":

![](/static/img/man_mail/en/mail-iphone4.jpg)

Enter your account information:

![](/static/img/man_mail/en/mail-iphone5.jpg)

Select "Next"

Select "IMAP" - "Next"

![](/static/img/man_mail/en/mail-iphone6.jpg)

Enter the Incoming Mail Server – mail.autistici.org

Enter your user name and password.

Enter the Outgoing Mail Server - smtp.autistici.org

Enter your user name and password:

![](/static/img/man_mail/en/mail-iphone7.jpg)

Select "Next".

Select "SMTP - smtp.autistici.org":

![](/static/img/man_mail/en/mail-iphone8.jpg)

Confirm smtp.autisitici.org is Primary and on.

Confirm other SMTP servers of Off:

Select "smtp.autistici.org On"

![](/static/img/man_mail/en/mail-iphone9.jpg)

Confirm Use SSL is on and set port to 587:

![](/static/img/man_mail/en/mail-iphone10.jpg)

Select "Done"

Select "Advanced":

![](/static/img/man_mail/en/mail-iphone11.jpg)

Confirm Use SSL is on and set port to 993:

![](/static/img/man_mail/en/mail-iphone12.jpg)
