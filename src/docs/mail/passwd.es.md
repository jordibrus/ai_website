title: Como cambiar o recuperar vuestra contraseña 
----

Como cambiar o recuperar vuestra contraseña
==========================

A/I ha preparado un servicio para aquellos de vosotros con muy mala memoria, que os permitirá recuperar vuestra contraseña al contestar una
simple pregunta...

Ya que la pregunta por defecto es sobre mascotas, el servicio se llama GATTI (“gatos” en italiano)

¡Es <u>muy importante</u> que elijas tu respuesta antes de perder la contraseña! (Sugerimos que lo hagas apenas tu cuenta sea activada).

Puedes hacerlo utilizando el [panel de usuario](https://accounts.autistici.org) de A/I

- - -

Cómo recuperar tu contraseña
----------------------------

### Paso 1 – LLama el gato

Ve a la [-Página para acceder a tu panel de usuario](https://accounts.autistici.org),
ententa poner tu nombre de usuario y tu contraseña y un nuevo enlace te va a
aparecer: clique sobre eso por acceder a la pregunta de recuperación!

### Paso 2 – Responde a tu pregunta de recuperación

Ahora verás la pregunta que has elegido cuando recibiste tu contraseña por primera vez. Una vez que hayas introducido la respuesta correcta,
verás un título diciendo:

*Ok. Tu nueva contraseña es RtYcYEFnVMvu. De cualquier modo, necesitarás cambiarla antes de poder usarla nuevamente...*

Esto significa que tienes una nueva contraseña, que es: *RtYcYEFnVMvu* y que DEBES cambiarla para acceder a tu casilla de correo.

### Paso 3 - ¡Cambia tu contraseña!

Para cambiar tu contraseña, simplemente entra en el panel de usuario [¡y sigue el link!](/docs/userpanel#passwordchange)
