title:  About this project, about us, about infrastructures and about the world
----

Frequently Asked Questions
==========================

What's this project about?
--------------------------

If you don't have a clue, first read our <a href="/who/manifesto">manifesto</a>; 
if you still have doubts and you've also read our faqs, well, 
[write to use](mailto:info@autistici.org). We'll try to answer.

How much bandwidth do we use?
-----------------------------

At the moment our servers use about 20-30Mb/s. However, considering the number of users we have, the bandwidth we 
have at our disposal is always insufficient. 

What Operating System do you use?
---------------------------------

[Debian](http://www.debian.org) GNU/Linux.

Have you got any documents about the servers configuration/installation?
------------------------------------------------------------------------

We wrote some extensive documentation about our infrastructure model back in 2005, that is still relevant in the
general lines, and still deserves a read (even if it is quite outdated in the details). 
We called it [R plan](/who/rplan/) back then because it was an unprecedented effort in defining ways to build a Resistant network. 

More up to date materials can be found by browsing our [Code](https://git.autistici.org/public) that is fairly good documented.
The "new" infrastructure is based largely on [Float](https://git.autistici.org/ai3/float), some piece of code we built recently,
that we are fairly satisfied with.

If you are interested in how we set up things or you would like to replicate our setup these are good places to start.

How long have you been online?
------------------------------

The first server was installed March, 3th, 2001, and we've been online since April, 19th, 2001; so that is 
our birthday. The project was officially launched at the [Hackmeeting 2001](http://www.hackmeeting.org/hackit01)

How many mail users do you have? How many websites and mailing lists do you host?
---------------------------------------------------------------------------------

We can't give the exact figures, but we host about 16000 mailboxes, 1500 websites, 5500 lists and 10000 blogs.

Who is legally liable for this project?
---------------------------------------

The "AI ODV Association" is the legal subject signing contracts for connection and owning the domains
related to the "A/I" project.


How can I support your project?
-------------------------------

If you want to support us, read the [Donate](/donate) section in our website.

How can I co-operate with this project?
---------------------------------------

One aspect we constantly need help with is translations. We would like this website and the how-tos and all the technical
documentation to be available in as many languages as possible. So if you master a language and you would like to help,
that's an awesome place to start (even if you only speak English but you are mother tongue). 
You can clone our [Website repository](https://git.autistici.org/ai/website), and send us some pull requests. 


Other than that the A/I collective gladly welcomes any new useful ideas (they don't have to be strictly technical 
in order to be useful ;). If you have any proposals, [write to us](mailto:info@autistici.org).

I am skilled in system administration/scripting/coding.
-------------------------------------------------------

The A/I collective doesn't have any particular needs for the administration of its servers, however, we invite you to use 
your skills and resources also to create small servers offering services for anonymity. If you want to do that using any of 
the pieces of software we created and [made available to the public](https://git.autistici.org/public), you might
find interesting bugs or improvements you could send to us in the form of a Pull Request. This would be a great place to start.

Other than that we appreciate any software released under GPL and useful for the safety of the data contained in our servers.

I am skilled in design, communication and media strategies.
-----------------------------------------------------------

We appreciate any help for explicitating our intentions: from graphics to concept presentation, 
from accessibility to mediascape impact, these are domains in which we have never been very good!
You can clone our [Website repository](https://git.autistici.org/ai/website), and send us some pull requests. 

I'd like to organize a benefit event/party/meeting for this project. May I? What should I do?
---------------------------------------------------------------------------------------------

Sure! If you contact us, we'll appreciate any benefit event for our project. Of course, events mustn't be 
commercial and must respect the principles in our [Manifesto](/who/manifesto). Other than that, we think that organizing
an event to support us is such a flattering compliment to us, our work, and really gives us good vibes to continue on.

I have a lot of unused bandwidth. Can this be helpful?
------------------------------------------------------

Yes, it can. Connectivity is very important, and unused bandwidth should always be used.
If you can and know how to do it ( [contact us](mailto:info@autistici.org) if you need help), you can install a server and offer any service 
you wish (proxy, anonymizer, mailing-lists, p2p...)

I know/own/work in a place which could house a project. Are you interested?
---------------------------------------------------------------------------
Sure :) [Contact us](mailto:info@autistici.org), and we'll think about it.

I'd like to install a server and offer some/all services offered by A/I. How can I do it?
-----------------------------------------------------------------------------------------

Good: it's a brilliant idea. We can only advise you to read the docs about the configuration of a 
[Debian](http://www.debian.org) server, and perhaps give you some tips. [Float](https://git.autistici.org/ai3/float) is also a good tool
we wrote that you could consider using. 
 
Read some pages in our [Code](https://git.autistici.org/public) and contact us for any suggestion :)

Can I link you in my site?
--------------------------
Sure. Especially if the description of the link is not insulting ;)

Who supports your project? Do you have any sponsors?
----------------------------------------------------

We don't have proper sponsors or supporters. To support our project, we annually ask our users for a donation,
and with their help we sometimes organize some benefit events.

How much do you pay for the services you offer?
-----------------------------------------------

Our services don't have a fixed price. Of course, since we have to provide for our servers infrastructure and 
connection, we do need money. We generally launch annual fundraising campaigns, asking for donations according to
each user's possibilities (hoping that groups and collectives manage to donate more).
For instance, 10 euros will be sufficient for mailboxes, 20 for mailing lists and if a group using a website 
reaches 50 euros, it would be wonderful.
However, even if you donate few euros, we'll be very grateful! If you want to know more, read our page about 
[donations](/donate).

Are donations tax-deductible?
-----------------------------

No: donations to A/I cannot be legally deducted from taxes. They could, if we were a legally recognized non 
profit organization, but to obtain such a status we'd have to undergo bureaucratic procedures we haven't 
considered sustainable sofar. If you think that such a development could be useful, let us know: 
it could perhaps give us the right input to rethink our status ;)

Do members (admins, designers, coders, lamers) earn money and/or get repayments for activities carried out within the project?
------------------------------------------------------------------------------------------------------------------------------

No: whoever co-operates with this project is a volunteer. We never receive payments/wages/repayments for the 
time we spend for this project.

I'm totally broke, can I have a mailbox in your servers anyway?
---------------------------------------------------------------

Sure you can; but actually, considering that we never have enough money, you could perhaps give us 5 euros, 
couldn't you? You can't? Alright: we hope we can survive anyway.

I can afford a small/large donation. What should I do?
------------------------------------------------------

Before you change your mind, since generosity is very flattering, read [donations](/donate) page now!

Are donations anonymous?
------------------------

It depends on the payment modality you choose: if you give us the money personally, or hide it in a CD you send 
by mail, they are. 
If you choose to pay by draft or by credit card, your bank will keep track of this transaction. 
However, we don't keep a record, but in our hearts ;), of the people who contribute money to our project. 
Unfortunately, we don't know yet how to receive online donations anonymously: can you suggest one?

Is it possible to find an agreement for a greater visibility of your web pages?
-------------------------------------------------------------------------------

If you are talking about exchanging a link or a banner with money, our answer is obviously **No**
We never accept advertising banners/links, from anyone. We accept donations from anybody, provided there's no 
condition, wether explicit or implicit.

I'd like to open a web space on the A/I servers, but I don't know if my project is suitable.
--------------------------------------------------------------------------------------------

First of all you should read our [manifesto](/who/manifesto) and our [policy](/who/policy).
After that, if you think your project is suited, go to the [sign-in](/get_service)" subscription form.

Some services aren't working! This is unacceptable!
---------------------------------------------------

We're sorry about that. But if you find it unacceptable, we advise you to use a different server.
Our priority is privacy and security, and sometimes that means that a service is down. 
Our collective always tries to do its best to keep everything functioning, 
but as you should have already understood, we are never paid for this service and sometimes problems are 
difficult to solve. 
Trust us, give us a little time, and we hope we'll improve our services even more. And this will also be 
possible through your donations.

I wrote you a mail more than a week ago and you haven't answered yet. How so?
-----------------------------------------------------------------------------

Managing this project does request time, but it is not a job for us (meaning that we are not paid for it): 
that's why you can receive an answer 10 minutes or 10 days 
after you've sent us a message. This can depend from the weather or from a locust invasion: you can never know!


I've tried to fill in the form for requesting a service, but something went wrong. What should I do?
----------------------------------------------------------------------------------------------------

Perhaps you requested a service without having an account with us: in this case, you first have to ask for a 
mailbox and then you can ask other services. Or perhaps you requested a blog but you don't have a reliable
mail account. In this case, you first need a mailbox with us or with [another provider](/links), and then 
you can have your blog. Or else there are some problems in your browser setting: if you experience such problems, please contact us at 
[info@autistici.org](mailto:info@autistici.org).

The 'comment' field in the form is compulsory. What should I write in it?
-------------------------------------------------------------------------

Any hint about why you're requesting this service and perhaps what you want to do with it.

Why should I trust you?
-----------------------

Trust actually depends on you: we have a confidence relationship with many users of ours, and we have a 
[manifesto](/who/manifesto) describing all the aims of this project, and the [privacy policy](/who/privacy-policy). 
But this is not a reason enough to entrust us your whole privacy. Even if we try
to prevent this, we could lose control over our servers anytime (due to a police seizure, to a casual interruption of our services or to a bug, 
for instance). We can't therefore grant that your data in our servers will never be violated and warmly 
invite our users to:

* download all their personal data
* encrypt their personal archives.

Do you keep logs connecting service users with personal identities?
-------------------------------------------------------------------

No. However, we can't stop you from using your real name and surname in the mail address you choose, if you so wish.
That would be a bad idea, but we can't know if you really did it, since we did not ask your real name in the first place.

What logs to connections do you keep? According to what principles?
-------------------------------------------------------------------

We don't keep any logs containing sensitive data.
Our servers keep no records of the IP addresses by users connecting to them.
We only keep logs for debugging and security purposes.
To learn more, read our [policy](/who/policy) and the [privacy policy](/who/privacy-policy).

What is the capacity of my mailbox?
-----------------------------------

Theoretically it is unlimited: so far, we have trusted our users common sense, and we expect a certain degree of 
self-management and responsibility by our users, who should know that our project is totally managed on a 
volunteering basis and is only supported by our users donations.

Are there any limits for attachment sending?
--------------------------------------------

Yes: about 10Mb incoming and outgoing.

I've received this error message:
---------------------------------

**4.7.1 <END-OF-MESSAGE>: End-of-data rejected: Recipient limit exceeded. Please check the message and try again.**
what does it mean ?

It means that you sent too many messages, so your account is temporary blocked.
Limits are:
100 recipients in a day
or
10 messages sent in an hour
If you are using your mailbox to send mails to a lot of recipients
maybe you should ask us of a newsletter or a mailing list service.

What mail client do you recommend for windows?
----------------------------------------------

The tools we advice are all licensed as [FOSS](http://www.gnu.org/philosophy/free-sw.html), 
usually under a [GNU/GPL license](http://www.gnu.org/licenses/gpl.html)
The mail client we most recommend is [Mozilla Thunderbird](http://www.mozilla.org/products/thunderbird/).

How shall I configure a mail client to receive and send mail?
-------------------------------------------------------------

For details about configuration, read [this](/services/mail).
You received the general configuration data with your activation mail, and you can find them 
[here](/docs/mail/connectionparms).

How do I send mail through TLS (encrypted protocol)?
----------------------------------------------------

You can find all necessary information in our [tech mail howto](/docs/mail/). 
Any contribution to extend and improve our howtos are welcome: write it and send it to us!

Is encryption absolutely necessary?
-----------------------------------

No, it isn't; but it is a good idea to encrypt you data. 
A message that hasn't been encrypted travels "in plain" and you don't need a brilliant computer 
scientist to intercept your mail and read it or change its content.
A/I provides both an online encryption system (https instead of http, whereby 's' means 'secure') 
and the encrypted protocols IMAP-SSL and POP3-SSL for downloading your mail.
These system **do not** grant total security, which can only be afforded by encrypting your mail with GPG. 
To learn more, read our [howto about privacy](/docs/mail/privacymail).

I'm receiving a lot of spam. What's happening?
----------------------------------------------

We do use some anti-spam filters, but they are never exact. Apart from being patient, you could use the spam 
filters available with several mail clients, for instance with Thunderbird.
And be sure that you have a filter that move the spam into the spam folder (see [filters](https://www.autistici.org/docs/mail/sieve))

Valid messages are marked as spam, why ? 
----------------------------------------

Send us (write to helpdesk@autistici.org) the complete headers of the message, hidden headers X-Spam-Status and X-Spam-Report 
explain why a message is tagged as spam.

How often do you send your newsletters?
---------------------------------------

A/I's newsletter is not sent regularly: this means you'll receive, once in a while, an e-mail 
messages from us. Usually our newsletters are about:

* Technical issues
* Updates about the project and services
* News we find particularly meaningful

We have lately sent our newsletters once every 2-3 months, and give more information in our [blog](https://cavallette.noblogs.org/)
and in your user panel.

I don't want to receive your messages any more. What should I do?
-----------------------------------------------------------------

Well, if you have a mailbox in our servers, you need to receive the few messages we send (sent by 
A/I). This is the only way we have to contact you directly, to keep you updated about services and catastrophes and to 
inform our community. You don't need a big effort to read them, so try to do it. At any rate, there are a lot of other free mail 
services you can use. There is not a single bit in our newsletters that is about marketing, we promise you. It's stuff you need
to know. 

How come do I only manage to send messages to inventati.org and/or autistici.org mail addresses?
------------------------------------------------------------------------------------------------

If you face such a problem while using webmail, please, notify it to us immediately. 
If you're using a mail client, probably you aren't using SSL correctly to connect to our SMTP server. 
Check your client configuration with our [mail howtos](/docs/mail).
 
I would like to unsubscribe from a mailing list: what should I do?
------------------------------------------------------------------

If you want to unsubscribe from a mailing list in our server, go to https://www.autistici.org/mailman/listinfo/LISTNAME 
(replacing LISTNAME with the particular list name, for example https://www.autistici.org/mailman/listinfo/test if the list is called
test@autistici.org). At the bottom of the page you'll find the option "unsubscribe", and if you don't know your password, 
you can ask for it to be sent to your address through the specific box in the same page.


I would like to close a mailing list I manage: what should I do ?
-----------------------------------------------------------------

Only we can close a mailing list, write to helpdesk@autistici.org the name of the list and if archives are enabled, 
if you want a copy of them.

I forgot the password to manage a mailing list. What shoud I do ?
-----------------------------------------------------------------

If the owner email is hosted on our server, just login in the user panel, there's a link to reset the password.
Otherwise write to helpdesk@autistici.org telling us the name of the list.

I'd like to open a mailbox on A/I's servers. What should I do?
--------------------------------------------------------------

Go to the dedicated web page and [sign in](/get_service).

Will you send me spam and/or marketing messages?
------------------------------------------------

No, we won't. Despite our antispam filters, you'll receive spam anyway. 
The only thing we can assure is that we won't give away (either for free or for commercial purposes) your mail address. 
But, as usual, we recommend you to be cautious yourself, when giving your address around.

May I use my mailbox for commercial purposes?
---------------------------------------------
If you use your private mailbox for your work, we have no problems (also because we never control the content 
of e-mail); but if you want a free mail address for your job and you've bumped into this project by chance, 
then our answer is No.

For further information, read our [policy](/who/policy) and our [manifesto](/who/manifesto).

Is my mailbox content safe, if I leave it in the server?
--------------------------------------------------------

No, it isn't. As any other server, also our servers can run into accidents that could undermine its security.
Besides, unlike other servers, our servers are potentially more under control. For instance, the content of your mailbox is
encrypted automatically with a personal key related to your user, that makes your mail unreadable even to us.

We do our best to protect our users' privacy, but this could be insufficient.
We therefore always invite everyone to download their mail and every other data and/or to encrypt their archives.

I can't login in the webmail, I got an error about wrong user or password.
--------------------------------------------------------------------------

Be sure that the username you are using is your complete mail address.
If you forgot your password, try using the recovery password that we recommended you to set up. 
If you don't have any of those, there is no way we can help you (we never asked you your name or telephone number so it is
practically impossible for us to know if you are actually the owner of a certain mailbox).

I can login in my panel, but not in the webmail
-----------------------------------------------

Verify that: 

* Your password isn't longer than 63 characters (a limit due to the authentication software we use). 
* You have logged into your account in the last year (we automatically deactivate accounts that haven't been used for 365 days).
* You have changed your password after the 1st of June 2019.


I can't access my mailbox after I've not used it for some months
---------------------------------------------------------------- 
Probably it has been deactivated, go to [Helpdesk](https://helpdesk.autistici.org)
and ask for re-activation (remember to use a different email address in the form to receive our answer).
If for any reason helpdesk is not available, [write to us](mailto:info@autistici.org).

All my mails have disappeared !!!!
----------------------------------

Are you sure you have not download them with a client ?
Or maybe your mailbox has been moved to another server, and mail are syncing, [check our blog for news](https://cavallette.noblogs.org)
Anyway, remember that we do not keep backup of your emails, so if your emails are so important, download them 
with a client.

I am the owner of a mailing list, but I can't remember the password to login in the administration panel.
---------------------------------------------------------------------------------------------------------

You can reset the password from your user panel. If this does not work, or you haven't a mailbox on our servers
you can go to [Helpdesk](https://helpdesk.autistici.org)" and ask us to have a new password.
[Write to us](mailto:info@autistici.org) if helpdesk is not available.

I am the owner of a mailing list, one of subscriber do not get any message from the list.
-----------------------------------------------------------------------------------------

Ask to that user to check his/her spam folder, maybe messages are there.

I receive more than one daily digest.
-------------------------------------

Contact the mailing list admin and tell him/her to modify the digest_size_threshhold parameters in the digest section of
the list control panel.

I can login in the panel but webmail link gives me an error. The password I'm using is correct, since I can read mails using a mail client.
-------------------------------------------------------------------------------------------------------------------------------------------

Probably you are using a password longer than 63 characters, or strange characters.
Choose a shorter password and avoid weird characters.

I've forgotten the password of the mailbox.
-------------------------------------------

We do not keep track of who request a service, so we can't know if you are the legitimate owner of the mailbox.
If you haven't se or don't remember the recovery question we can't help you.

I've forgotten my password but it's still in thunderbird.
---------------------------------------------------------

See here how to recover it [here](https://support.mozilla.org/en-US/questions/1005341).

I've lost my OTP code 
---------------------

Try to login with a wrong code, and then click on the "Forgot your password ?" link answering the security 
question you should have setup you can change your password and you will login into the user panel without OTP code, 
and then you can re-enable 2FA if you want.  
If you don't remember the security answer write to helpdesk@autistici.org

Can I change the address of my mailbox ?
----------------------------------------

No, but you can create a alias in your user panel and set it as default identity in the webmail/client.

Can I switch an alias to make it may main address ?
---------------------------------------------------

No, it's complicated and we can't help you moving messages/addressbook, so ask yourself if you really need this. 

I want to close my account.
---------------------------

You can do it from your user panel.

How can I set the recovery question and I can I change my password?
-------------------------------------------------------------------

In your user panel click on the "Account Management" button.

How many mails can I send every day ?
-------------------------------------

Actual limits are: 10 mail/hour for a maximum of 100 recipient/day. Contact us if you need more or request a mailing list/newsletter.

I need a counter. What should I do?
-----------------------------------

You can consult our [stats](https://stats.autistici.org/)", which include every website.

May I use a counter or other commercial services in my webpage?
---------------------------------------------------------------

No! Commercial counters are BAD. Apart from being commercial (which would be a reason enough), external conters 
make our efforts not to violate surfers' privacy vain.
These counters record data bases with personal data and IP addresses, which are then sold away. We don't like this.
As regards other commercial services, the same principle applies. You may have good reasons, and we can discuss 
about them, but we generally don't want commercial services in our spaces.

What size can my web space reach?
---------------------------------

There's no specific rule about this: let's say we don't set any limits, unless a space troubles a whole server. 
If you want to upload "heavy" materials (mainly audio and video), we recommend you to host them in dedicated 
space such as [Arkwiki](http://www.arkiwi.org).

Can I upload copyrighted material, such as mp3 files and films, thus violating their rights?
--------------------------------------------------------------------------------------------

No, we're sorry. Our servers provide services to thousands of people
and we don't want to incur in useless problems as regards copyright violation.
You can use P2P for that stuff.

I have a lot of "heavy" stuff to upload (mp3 files and videos under free licenses). Wher shall I put it?
--------------------------------------------------------------------------------------------------------

Before you upload these files, you'd better contact us and check that there's enough space in the HD. At any rate, 
the right place to upload your material is your WebDAV space.

What application can I use to upload my web page?
-------------------------------------------------

Any WebDAV client, as detailed [in our webdav howto](/docs/web/webdav)

I need a blog. What should I do?
--------------------------------

Use [Noblogs](https://noblogs.org)", our blog platform.

I need a CMS: what should I use?
--------------------------------

We do not offer PHP anymore, so no CMS, only static sites...

What database software is installed in A/I's servers?
-----------------------------------------------------

MySql Server.  If you need a graphical frontend consider [adminer](https://www.adminer.org)

Can I use commercial banners in my site?
----------------------------------------

No, you can't. And if you do, you'll lose our respect (and, if you insist, your web site).

How can I set my favicon for my site ?
--------------------------------------
You have to use this html code in the HEAD section of your pages:

```python
 &lt;link rel="shortcut icon" type="image/x-icon" href="/path/to/favicon.ico" /&gt;
```
where /path/to is the relative path of your website.

I can't delete some directories in my WebDAV space.
---------------------------------------------------

Check that your WebDAV client shows hidden files, and delete the file .htaccess before removing the directory.

I can't register on Noblogs.
----------------------------

You have to use an email address registered on our server or other privacy related servers.
Commercial mailbox are not allowed.

Can you add a plugin/template to Noblogs ?
------------------------------------------

No, we are not going to install other plugins/templates at the moment.

How can I delete my blog ?
--------------------------

From the administrative interface, "Tools" -> "Delete Site".
A mail will be sent to your address to confirm

I've seen your wonderful +kaos sweaters and I'd like to have one. What should I do? How much do you ask for them?
-----------------------------------------------------------------------------------------------------------------

Those sweaters were all produced and distributed in 2002.
Unfortunately we have no time to manage wares and distribution. As for our whole merchandising, the production
of sweaters will be linked to particular events, such as our <[Kaos Tour](http://kaostour.autistici.org).

I've seen your brilliant T-shirt with a locust on it. How can I get one?
------------------------------------------------------------------------

That was made for the 2004 spring-summer season! Perhaps we'll distribute some T-shirts in our upcoming
events, but we haven't developed an on-demand distribution system yet.

I've seen you stickers and would like to have some: what should I do?
---------------------------------------------------------------------

The easiest solution is to download the relevant file and to print them near where you live! 
Look for what you want in our [Propaganda](/who/propaganda) section.

May I print your stickers on my own?
------------------------------------

Sure! This is also the only sure way to get them ;) 
Download the files [here](/who/propaganda).

I'd like to interview some of you: is it possible?
--------------------------------------------------

Sure, but we can't assure anything. We generally prefer to be interviewed via e-mail and
the text of the article must be released under a Creative Commons license.

Can I write an article about you?
---------------------------------

Of course. However, we would like to be informed about it.

I'd like to invite you to a TV show...
--------------------------------------

We don't usually like TV, but if you are OK with us wearing a Mickey Mouse mask, that could be possibly be done.


