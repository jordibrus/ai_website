title: Blog e censura

----

Blog e censura
==============

L'idea di filtrare i contenuti a disposizione di chi accede a Internet è nata principalmente con la scusa di proteggere i bambini dal
contatto con contenuti "pericolosi". In un secondo momento questo tipo di tecnologia è stato usato per impedire l'accesso in particolari
contesti: biblioteche, scuole, posti di lavoro. Infine siamo arrivati agli stati che bloccano l'accesso dei loro cittadini ai contenuti che
ritengono inappropriati, per ragioni politiche, morali o solo per semplice idiozia.

Tutte le tecnologie di filtraggio si basano di solito su "liste nere" o sulla ricerca di "parole chiave" in base alle quali si individuano
le risorse da bloccare. Accanto a questi ci sono delle liste di nomi di dominio e di URL che vengono classificati e catalogati in modo da
bloccare determinate categorie di contenuti.

Quando un utente prova ad accedere a una risorsa, i programmi di filtraggio controllano (con i sistemi descritti prima) che questa non
appartenga a una di quelle "proibite" prima di permettere l'accesso.

Questo sistema ha due svantaggi: quello di bloccare anche siti che non hanno contenuti "pericolosi" e quello di non bloccare siti che hanno
contenuti "pericolosi". Per esempio divenne famoso il caso del blocco del sito della Casa Bianca in quanto sulla pagina web compariva la
parola "coppia" (couple) in riferimento alla "coppia presidenziale" e (in inglese) la parola "couple" sta anche a significare
"accoppiamento" una parola che viene bloccata da alcuni filtri tarati sui contenuti pornografici.

Oltretutto c'è anche il problema che la maggior parte dei programmi di filtraggio non rende pubbliche le liste che utilizza e quindi questo
rende possibile qualsiasi tipo di abuso. Molto spesso stati e governi dittatoriali usano questi sistemi per impedire ai loro cittadini di
accedere a informazioni che potrebbero creare problemi e così tra le liste delle risorse filtrate finiscono i siti delle organizzazioni per
i diritti umani, dei sindacati e dei partiti di opposizione, dei gruppi politici più radicali e dei dissidenti.

Per aggirare un filtraggio e un controllo gestito in modo centralizzato da organismi governativi, si possono usare vari sistemi. In generale
si tratta di reindirizzare le richieste provenienti dai computer che si collegano a Internet da paesi "censuratori" attraverso computer che
invece sono in paesi meno dittatoriali. In altre parole si tratta di trovare un computer che faccia da "intermediario" tra il computer
censurato e quello che si vuole raggiungere.

Non sempre questi sistemi sono facili da utilizzare e non è detto che il sistema che va bene in un caso vada bene anche in un altro; inoltre
questi sistemi provocano un aumento del traffico a carico del computer che fa da intermediario con tutti i problemi che una cosa del genere
comporta in termini di costi, di sicurezza e di velocità. Va tenuto anche in debito conto il possibile abuso di un sistema del genere che
potrebbe essere utilizzato anche da persone che se ne servono per motivi diversi da quelli della libertà di comunicazione.

Altri fattori da tenere presenti prima di attivare un servizio del genere riguardano la tipologia dei futuri utenti, le loro conoscenze
tecniche, il modo in cui accedono alla Rete, i servizi che chiedono.

Infine c'è la necessità di sviluppare rapporti di fiducia in modo tale che il servizio sia considerato "affidabile" da persone che
potrebbero avere seri problemi con le autorità del proprio paese. In alcuni casi potrebbe essere considerato anche reato usare dei sistemi
di aggiramento delle barriere poste dai governi.

Vediamo adesso alcuni dei sistemi utilizzabili per provare ad aggirare la censura.

VIA WEB
-------

Il sistema più semplice utilizza le pagine web: alcune pagine web hanno un form in cui è possibile inserire l'indirizzo del sito che si
vuole visualizzare. In questo caso la richiesta della pagina verrà fatta dal sito e non dall'utente e quindi non ci sarà alcun contatto tra
il sito web censurato e l'utente che lo ha richiesto.

Il vantaggio di questo sistema è che non è necessario installare alcun software e che si può utilizzare anche da un net-café o dal posto di
lavoro. Chiaramente se a essere bloccato è proprio il sito che fa da intermediario, il sistema non funziona.

Esistono vari sistemi di intermediazione; ecco una breve lista:

- <http://www.unipeak.com/>
- <http://www.anonymouse.ws/>
- <http://www.proxyweb.net/>
- <http://www.webwarper.net/>
- <http://www.the-cloak.com/>

Bisogna tener presente che molti di questi siti non crittano il traffico, che così può facilmente essere letto da chiunque interessato a
spiarlo.

In definitiva il sistema via web è consigliabile solo agli utenti che hanno bisogno di un basso grado di riservatezza e/o dagli utenti che
non hanno un alto grado di rischio.

D'altra parte, per installare un sistema di aggiramento occorre un minimo di esperienza tecnica ma, soprattutto, risorse che permettano di
far funzionare il sistema senza troppi rallentamenti (che comunque sono inevitabili); la cosa migliore sarebbe installare questi sistemi su
computer "privati" la cui esistenza è nota solo a pochi utilizzatori, stabilendo un rapporto di fiducia tra chi offre il servizio e chi lo
usa, anche pochi, evitando così i problemi e i rischi di mettere una risorsa a disposizione di tutta la Rete. In questo modo è anche più
facile che il sistema non venga bloccato dai filtri che invece colpiscono più facilmente i sistemi conosciuti da tutti.

Dal punto di vista tecnico un sistema di aggiramento può essere personalizzato in modo ottimale per esempio cambiando il numero di porta sul
quale gira il server web e usando il protocollo SSL (Secure Sockets Layer) che critta la comunicazione: i siti che usano questo protocollo
si riconoscono perché vi si accede con un indirizzo del tipo "https" e non "http". Un altro sistema può essere quello di creare, a livello
di root del server, una pagina qualsiasi per usarla come aggiramento e crittografarne il nome; in tal modo un controllore non sarebbe in
grado di risalirvi, in quanto la richiesta mostrerebbe solo l'indirizzo del server e non anche quello della pagina.

Ecco alcuni programmi che possono servire per questi scopi:

- [Uno script CGI che funziona da proxy http o ftp](http://www.jmarshall.com/tools/cgiproxy)
- [Il programma "circumventor"](http://www.peacefire.org/circumventor/simple-circumventor-instructions.html) (purtroppo solo per windows)
  è di facile installazione e trasforma il proprio computer in un proxy server.
- [Un proxy server basato su web](http://ice.citizenlab.org/projects/phproxy)

La scelta migliore è quella di un programma di aggiramento che si basi sulla crittografia e che sia gestito da un privato col quale sia
instaurato un rapporto di fiducia reciproco e che abbia abbastanza risorse per far funzionare il programma senza rallentare troppo i
servizi.

Questo tipo di soluzioni non garantisce necessariamente l'anonimato di chi lo utilizza in quanto è sempre possibile (se le comunicazioni
avvengono in modo non crittografato) intercettare e leggere gli scambi tra l'utente e il server. Quindi anche se il programma funziona,
ovvero permette di aggirare i filtri posti dalle autorità, questo non impedisce alle autorità stesse di venire a conoscenza di cosa fanno
gli utenti del programma.

Talvolta i programmi di aggiramento usano qualche tecnica di "offuscamento" per rendere difficile il riconoscimento degli indirizzi
visitati, ma questo non impedisce comunque l'intercettazione delle comunicazioni che continuano ad avvenire "in chiaro".

Per ovviare al problema dell'intercettazione, alcuni proxy web permettono un collegamento tramite SSL (crittato); in questo modo un
eventuale controllore potrebbe solo constatare che un determinato utente si è collegato a un sito web su cui è attivo un programma di
aggiramento, ma non potrebbe conoscere le informazioni trasmesse e ricevute dall'utente.

Chiaramente anche le informazioni più sicure possono essere intercettate dal gestore del sito web sul quale gira il programma di
aggiramento... e, in alcuni casi, le autorità possono accedere ai file di log di questi siti o mettere in atto tecniche di hackeraggio che
permettono di intercettare la comunicazione.

Proxy server (vedi anche [Blog e anonimato](/docs/blog/blog_anonimo))
----------------------------------------------------------------------------------

I proxy server sono dei server che si collocano tra il client dell'utente (ad esempio un web browser) e il server web di destinazione e che
fungono da "deposito" in cui archiviare una serie di dati per renderli disponibili all'utente finale senza che questo debba necessariamente
collegarsi al web server richiesto.

Di solito vengono comunemente usati in diverse situazioni: per una maggiore sicurezza, per garantire la riservatezza, per velocizzare gli
accessi, per filtrare le informazioni.

Per usare un proxy server, l'utente finale deve configurare il suo programma inserendo l'indirizzo IP (o il nome) del server e la porta che
questo utilizza. Questi cambiamenti sono difficili da applicare se si usano computer pubblici (in un Internet café) o di lavoro.

Vantaggi e svantaggi: ci sono numerosi programmi tra i quali scegliere che possono far funzionare un proxy server "privato" e ci sono
diversi proxy server pubblici. D'altra parte la maggior parte dei proxy server non permettono la crittografia e, in molti casi, non è
possibile modificare le preferenze del browser per usare un proxy server pubblico. Inoltre in alcuni paesi potrebbe essere vietato del tutto
l'uso di un proxy server.

Ecco alcuni software disponibili:
- [Squid](http://www.squid-cache.org): un proxy server gratuito che può funzionare anche in modo sicuro con stunnel (vedi sotto)
- [Stunnel](http://www.stunnel.org)
- [Aardvark](http://ice.citizenlab.org/projects/aardvark)
- [Privoxy](http://www.privoxy.org): un proxy server con avanzate capacità di filtraggio per grarantire la riservatezza
- [OpenSSH](http://www.openssh.com): questo protocollo per la comunicazione crittata ha anche al suo interno un sock proxy

Alcuni dei siti web che con elenchi di server proxy pubblici:

- <http://www.samair.ru/proxy/>
- <http://www.antiproxy.com/>
- <http://tools.rosinstrument.com/proxy/>
- <http://www.multiproxy.org/>
- <http://www.publicproxyservers.com/>
- <http://www.web.freerk.com/proxylist.htm>

Un software per la gestione di un proxy:

- <http://proxytools.sourceforge.net>

In alcuni casi il sistema usato dai proxy per aggirare i filtraggi è quello di lavorare attraverso una porta non standard (una porta è una
connessione logica usata da uno specifico protocollo di comunicazione, per esempio – di solito – il protocollo http, quello del web, usa la
porta 80). In questi casi se il sistema blocca la porta 80 è possibile far passare la comunicazione attraverso un'altra porta e aggirare in
tal modo il blocco.

Qualche riflessione sulla sicurezza
-----------------------------------

I punti deboli dei proxy server, dal punto di vista della riservatezza delle comunicazioni sono:

1. le informazioni tra il computer e il proxy viaggiano "in chiaro" e sono quindi leggibili;
2. il proxy archivia l'Indirizzo IP dell'utente e questo potrebbe identificarlo;
3. tutto quello che passa per il proxy server è facilmente intercettabile dal gestore del proxy

Per questi motivi non sarebbe molto consigliabile usare un proxy server pubblico anche se funzionasse bene per aggirare i filtri in quanto
poco sicuro per la riservatezza. Inoltre, in certi casi, alcuni programmi risolvono gli indirizzi IP direttamente e senza passare per il
proxy e in questo modo resta traccia della richiesta spesso proprio sui computer DNS delle autorità governative che gestiscono i filtri.
Consigliamo quindi l'uso di proxy sever pubblici solo nel caso sia necessaria una riservatezza minima.

Un sistema migliore è quello del "port forwarding" (detto anche "Tunneling"), un sistema che permette di impacchettare le informazioni –
anche quelle "in chiaro" – all'interno di un "contenitore" crittato. Per usare questo sistema è necessario avere un programma software che
crei un "tunnel" (un canale di comunicazione) tra il nostro computer e un altro non filtrato. In questo modo tutti i servizi del nostro
computer useranno per comunicare quel "tunnel" protetto dalla crittografia. Sono disponibili diversi programmi, sia gratuiti che a
pagamento, e anche diversi servizi (gratis o a pagamento) che usano questo sistema di comunicazione. Spesso i servizi gratuiti sono pagati
da banner pubblicitari che viaggiano fuori dal tunnel e che possono far capire se vengono intercettati che si sta usando un tunnel.

Ecco alcuni di questi servizi:

- <http://www.http-tunnel.com/>
- <http://www.hopster.com/>
- [http://www.htthost.com](http://www.htthost.com/)

Vantaggi e svantaggi del sistema di tunnel: il vantaggio maggiore è che la comunicazione avviene in forma crittata e che è possibile usare
anche altri protocolli oltre a quello web; inoltre anche chi non ha contatti diretti personali può acquistare un servizio del genere. Lo
svantaggio è che i tunnel possono essere conosciuti e quindi filtrati, che non si possono usare da computer pubblici o di lavoro e che
spesso il sistema non è di facile uso per un utente medio. Inoltre questo sistema non garantisce l'anonimato di chi lo usa.

Altri sistemi di comunicazione riservata
----------------------------------------

La differenza principale tra i sistemi di aggiramento e quelli di anonimato è che i primi non sono centrati sulla riservatezza della
comunicazione come i secondi.

La comunicazione anonima tenta di garantire la riservatezza del suo utilizzatore anche nei confronti dei sistemi usati per l'anonimato,
mentre l'aggiramento si concentra maggiormente sulla necessità di evitare i filtraggi della comunicazione e quindi richiede un certo grado
di "invisibiltà" ma non necessariamente l'anonimato totale. Chiaramente un sistema di anonimato funziona anche come sistema di aggiramento
dei filtri.

L'uso di sistemi di anonimato è limitato ai computer su cui si hanno i privilegi per installare determinati programmi e quindi non può
essere usato da computer pubblici o di lavoro. Inoltre, questi sistemi rallentano la velocità dei collegamenti.

D'altra parte le autorità possono arrivare a bloccare l'uso di sistemi di comunicazione anonima e più un sistema è conosciuto maggiori sono
le probabilità che venga bloccato. Inoltre, come accade anche in altri casi, il sistema può essere messo sotto sorveglianza e i suoi
utilizzatori possono attirare l'attenzione su di sé.

Vantaggi e svantaggi: questi sistemi garantiscono sicurezza e anonimato e possono essere usati anche per comunicazioni diverse da quelle che
viaggiano via web. La comunità che si è formata intorno ai sistemi di crittografia e anonimato è ampia ed è facile trovare software e aiuto.
Lo svantaggio è che questi sistemi non sono stati creati per aggirare i filtri, che sono molto conosciuti e che non possono essere usati da
computer pubblici o di lavoro.

Software:

- [Tor](https://torproject.org/) è un network di tunnel virtuali che permette un certo grado di sicurezza e anonimato nell'uso di Internet.
- [JAP](http://anon.inf.tu-dresden.de/index_en.html) è un sistema (ancora imperfetto) che permette di collegarsi a siti web in modo
  anonimo in quanto le richieste vengono smistate (in forma crittata) a diversi computer chiamati mixes.
- [Freenet](https://freenetproject.org/) è un software libero che permette di entrare all'interno di una rete formata da tutti gli
  utilizzatori del programma che garantisce l'anonimato.

Per concludere
--------------

La scelta del sistema da usare per aggirare i filtri dipende da molti fattori: il grado di sicurezza, le capacità tecniche dell'utente, la
necessità di maggiore o minore anonimato e il grado di rischio che si corre o che si intende correre. Va tenuto sempre presente che la
controparte ha anche strumenti per bloccare molti dei sistemi di aggiramento.
