title: Anonymity Howtos
----

Anonymity howtos
================

These days there's a lot of talking about high-level control systems
tracing, logging and classifying endless data as regards activities by
anybody entering the Web, that is about systems blatantly violating the
fundamental right/need to privacy.

The colleagues sitting next to you or your boss can easily monitor
whatever you're doing in the Web, commercial websites trace your
activities for marketing analysis without asking for your consent, your
mail can be read by anybody who can access your providers boxes, etc.

If you want to avoid this, if you don't want your online activities to
be traced and recorded, you can surf the Web through an anonymizer such
as Tor and encrypt your e-mail messages with GPG.

To help you protect your privacy, we have decided to publish in this
section some useful howtos and links.

- [A general guide to protecting you privacy when using your
  mail](/docs/mail/privacymail "Privacy Mail")
- [Anonymous browsing and hidden services](/docs/anon/tor)
- [Email Encryption](http://www.gnupg.org/documentation/guides.en.html
  "GnuPG guides")
- [Anonymous mail](/docs/anon/remailer "Anonymous Mail")
