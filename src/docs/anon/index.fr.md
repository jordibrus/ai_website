title: Howtos Anonymat 
----

Howtos Anonymat
===============

À notre époque il y a beaucoup de débats autour des systàmes qui tracent, log et classe une quantité de donnée sans fin sur les personnes
connecté à Internet, ceci concerne ce genre de violation ouverte des droits/besoins fondamentaux à la vie privée.

Vos collàgues assis à coté de vous ou bien votre patron peuvent facilement espionner ce que vous faites sur le Web, les sites commerciaux
trace votre activité pour des analyse marketing sans votre consentement, vos mails peuvent être lu par toute personne accédant à votre "box"
Internet, etc.

Pour éviter cela, si vous ne voulez pas que votre activité en ligne soit enregistrée et tracée vous pouvez naviguer sur le web à travers un
systàme d'anonymisation tel que Tor et chiffrer vos mails avec GnuPG.

Pour vous aider à protéger votre vie privée, nous avons décidé de publier ici quelques liens et recettes pratiques.

- [Notes generaux pour une mail privé](/docs/mail/privacymail "Privacy Mail")
- [Navigation anonyme et service onions](/docs/anon/tor "Guida Tor")
- [Chiffrement mail](http://www.gnupg.org/documentation/guides.en.html "GnuPG guides")
- [Mail anonyme](/docs/anon/remailer "Anonymous Mail")


