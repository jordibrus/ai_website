title: Tor Howto
----

Tor How-to
==========

One method we strongly advise for surfing the web anonymously is
[Tor](https://torproject.org), a network of virtual tunnels allowing people to
browse the Web without revealing their identity.

To surf the net anonymously using Tor, you have to install [Tor
Browser](https://www.torproject.org/projects/torbrowser.html.en) (available for
all operating systems): a browser configured for anonymous browsing.

You can find detailed instructions [here](https://torproject.org/documentation.html).

Consider however that Tor by itself is not all you need to maintain your anonymity.
There are risks you need to be aware of and adjustments you need to make, all
listed [here](https://www.torproject.org/download/download.html.en#Warning).


Accessing A/I via Tor Hidden Service
------------------------------------

Most of our services are available as a [Tor Hidden Service](https://www.torproject.org/docs/hidden-services.html), at the following
address: [autinv5q6en4gpf4.onion](http://autinv5q6en4gpf4.onion/)

Currently the available services include the main site (webmail, WebDAV access, etc.), email (SMTP for deliveries and POP/IMAP), Jabber and
IRC, all of which are available at the above address.

Since .onion addresses are not particularly mnemonic, it is possible to retrieve the Hidden Service address with a DNS query:

    $ dig  +dnssec +noauth +noquestion +nocmd +nostats onion.autistici.org TXT @8.8.8.8
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 49449
    ;; flags: qr rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags: do; udp: 512
    ;; ANSWER SECTION:
    onion.autistici.org.    9600    IN  TXT "autinv5q6en4gpf4.onion"
    onion.autistici.org.    9600    IN  RRSIG   TXT 7 3 9600 20140110201148 20131211201148 24242 autistici.org. jshkdJJbHpxE0AzHcbQnr2mk75I/qawzLbObVX2A7lw79Sa5UEIjzHfl 75Vchn0095k9KTJqW2Y9yImxMTDuu3yXP1rmTzd9UXpEA7YFyPP5yOjU YUPS9BdzVOzYK9RsZSAOPom5fziDLzatcruI+/bPILbOOgR9vim/pZKr 0XI=

The thing to notice is the answer validated flag "ad" has been set. This example is using google public dns, however we recommend using a
local validating resolver as explained [here](/docs/dnssec).
Note: figuring out which .onion address to connect to is the subtle part of using an Hidden Service, we advise you to securely access this
web page via SSL, verify the SSL certificate, and copy the .onion address to a local file or some other safe place.


If you feel more old-fashioned, you can also find a list of public proxies here:

- <http://www.freeproxy.ru/en/free_proxy/>
- <http://www.samair.ru/proxy/>

