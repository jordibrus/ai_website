title: Note sulla privacy degli utenti
----

Note sulla privacy degli utenti
===============================

A/I fa del suo meglio per evitare di mantenere, sui suoi server, dati che possano permettere di identificare il navigatore. Per questo è
importante che il webmaster sia attento alle funzionalità del software che installa: ad esempio, molti contatori di visite, come molti
software per CMS, registrano gli IP dei visitatori (indipendentemente dalle impostazioni globali dei server web di A/I), ed è importante
controllare che queste funzioni siano disabilitate.

Ricordiamo che i server ospitano molti servizi e sono a oggi un importante strumento di comunicazione per migliaia di attivisti. Speriamo che
questa considerazione aiuti a ridurre i problemi che un uso non oculato dello spazio potrebbe causare.

Resta inteso che il server conserva esclusivamente i log strettamente necessari a operazioni di debugging.
