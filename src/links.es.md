title: Colectivos radicales tecnológicos
----

Colectivos radicales tecnológicos
---------------------------------

### Servidores anónimos e independientes que ofrecen correo electrónico y otros servicios con políticas similares a los nuestros

- [ecn.org](http://www.ecn.org/) (ECN es un lugar para relacionarse y estar en contacto con gente a la que los cambios profundos en nuestras sociedades han dispersado, un lugar donde encontrar personas que no se rinden a la uniformidad ideológica y marginalidad actual, entre personas que buscan crear un movimiento real, capaz de camnbiar las cosas - italia)
- [indivia.net](http://indivia.net/) (Listas de correo, streaming de audio, alojamiento web, IRC, correo electrónico, foros, DNS dinámico en un servidor independiente verde cuyo nombre significa "ensalada de endivias" - italia)
- [oziosi.org](http://www.oziosi.org/) (Oziosi.org reconoce la pereza, la lentitud y la ociosidad a la que tranquilamente pueden aspirar hombres y mujeres - italia)
- [free.de](http://www.free.de/) (Un proyecto alemán histórico que ofrece cuentas de correo, listas de correo, y alojamiento web para activistas - alemania)
- [resist.ca](http://resist.ca/) (El colectivo Resist! es un grupo de activista de Vancouver que trabaja para proveer servicios técnicos y comunicaciones, información y educación para la comunidad activista. El colectivo Resist! (Resist!) y el proyecto resist.ca crecieron a partir del viejo colectivo TAO - canadá)
- [riseup.net](http://riseup.net/) (riseup.net provee de correos electrónicos, listas y alojamiento web para tod\*s aquell\*s trabajando en el cambio social liberador. un proyecto para crear alternativas democráticas y prácticas de auto-determinación a través del control seguro de los medios de comunicación - u.s.a.)
- [squat.net](http://squat.net/) (squat!net es una revista internacional con las casas okupadas como tema principal, ofreciendo servicios de internet a espacios liberados - Países Bajos)
- [so36.net](http://so36.net/) (servidor independiente - alemania)
- [nadir.org](http://nadir.org/) (Un proyecto en pos de una revisión de los principios de la Izquierda a través de la creación de espacios de comunicación e información - alemania)
- [aktivix.org](https://aktivix.org/) (independent server offering email, mailing lists and VPN)
- [boum.org](http://boum.org/) (servidor anónimo e independiente - colectivo radicado en francia)
- [shelter.is](https://shelter.is) (tech collective providing solidarity driven privacy friendly services)
- [espiv.net](https://espiv.net) (Cybrigade is an autonomous collective with main point of report, social fights and how these are expressed in cyberspace, cybrigade is managing espiv.net's services. Providing e-mail accounts, mailing lists, blogs - greece)

  
### Una lista incompleta de proyectos con los cuales compartimos algo...

- [freaknet.org](http://www.freaknet.org/) (un laboratorio para experimentar con tecnologías de la información - catania, scicilia, italia)
- [interactivist.net](http://interactivist.net/) (un esfuerzo colaborativo, un recurso de comunicación activista, un proyecto de medios independientes y un proyecto para compartir tecnologías - u.s.a.)
- [mutualaid.org](http://mutualaid.org/) (un servidor para comunidades radicales - u.s.a.)
- [nodo50.org](http://nodo50.org/) (un servidor autónomo - españa)
- [tao.ca](http://tao.ca/) (tao.ca es un grupo anarquista de un pequeño colectivo de área de Toronto conocido como OAT, que evolucíonó desde el TAO en Toronto y está relacionado con otros grupos activistas como resist.ca e interactivist.net - canadá)

