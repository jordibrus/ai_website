title: Kompromisslos freiheitliche Technik-Gemeinschaften 
----

Kompromisslos freiheitliche Technik-Gemeinschaften
--------------------------------------------------

### Anonyme und unabhängige Server, die E-Mail-Konten und weitere Dienste, die unseren Idealen ähneln, anbieten

- [ecn.org](http://www.ecn.org) (ECN ist ein Ort der Transparenz, Verbindungen und möglichen Neuzusammensetzung für Menschen, die durch tiefgreifende Veränderungen in unserer Gesellschaft zersplittert und zerstreut wurden, für Menschen, die sich weder dem aktuellen 'Mainstream', noch den Gegenströmungen ideologisch verbunden fühlen, für Menschen, die wirklich etwas bewegen wollen und den heutigen Stand der Dinge ändern wollen - Italien)
- [indivia.net](http://indivia.net) (Mailinglistem, Audio-Streaming, Web-Hosting, IRC-Chat, E-Mail-Konten, Foren und Dynamisches DNS mit einem unabhängigem, laubgrünem Server, dessen Name auf Endiviensalat anspielt - Italien)
- [oziosi.org](http://www.oziosi.org) (Oziosi.org erkennt Trägheit, Langsamkeit und Müßiggang als die natürlichen Zustände an, die sich freie Frauen und Männer still und heimlich wünschen - Italien)
- [free.de](http://www.free.de) (ein fast schon historisches deutsches Projekt, das E-Mail-Konten, Mailinglistem und Speicherplatz im Internet anbietet - Deutschland)
- [resist.ca](http://resist.ca) (die Resist!-Gemeinschaft ist eine Gruppe aus Vancouver, die allgemein technische und speziell kommunikative Dienstleistungen und Informationen zur Verfügung stellen. Die Resist!-Gemeinschaft und resist.ca sind aus der älteren TAO-Gemeinschaft in Vancouver entstanden - Kanada)
- [riseup.net](http://riseup.net) (Riseup.net bietet E-Mail-Konten, Mailinglistem und Web-Hosting für alle, die an einer freiheitliche-sozialen Änderung arbeiten. Das Projekt möchte demokratische Alternativen aller Art schaffen und mehr Selbstbestimmung bezüglich sicherer Kommunikation erlauben - USA)
- [squat.net](http://squat.net) (Squat!net ist ein internationales Internet-Magazin, das Hausbesetzungen thematisiert und Speicher und andere Dienste im Internet anbietet - Niederlande)
- [so36.net](http://so36.net) (Unabhängiger Server - Deutschland)
- [nadir.org](http://nadir.org) (Ein Projekt, das eine Überarbeitung der Grundsätze von Linken durch die Schaffung von Kommunikations- und Informationsraum erreichen möchte - Deutschland)
- [boum.org](http://boum.org) (Unabhängiger und anonymer Server - Gemeinschaft aus Frankreich)
- [aktivix.org](https://aktivix.org/) (Unabhängiger Server, der E-Mail-Konten, Mailinglistem und VPN anbietet)
- [shelter.is](https://shelter.is) (tech collective providing solidarity driven privacy friendly services)
- [espiv.net](https://espiv.net) (Cybrigade is an autonomous collective with main point of report, social fights and how these are expressed in cyberspace, cybrigade is managing espiv.net's services. Providing e-mail accounts, mailing lists, blogs - greece)


### Eine unvollständige Liste von Projekten, mit denen wir etwas gemeinsam haben

- [antifa.net](http://antifa.net)
- [freaknet.org](http://www.freaknet.org) (Eine Arbeitsstätte, die mit Kommunikationstechnologien experimentiert - Catania, Italien)
- [interactivist.net](http://interactivist.net/) (Ein Gemeinschaftsprojekt, ein Ort zum Austausch, ein unabhängiges Medien-Projekt und ein Projekt, das dem Weitergeben von technischen Fertigkeiten dient - USA)
- [mutualaid.org](http://mutualaid.org) (Ein Server für revolutionäre Gemeinschaften - USA)
- [nodo50.org](http://nodo50.org) (Ein unabhängiges Projekt, das über Telematik informiert - Spanien)
- [tao.ca](http://tao.ca) (tao.ca ist ein anarchistisches Projekt in Toronto, betrieben von einer Gemeinschaft, die als TAO bekannt ist. Es gibt Verbindungen zu anderen Projekten, wie resist.ca und interactivist.net - Kanada)

